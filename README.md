# DOF Renderer

A *real-time* renderer focused on **rendering DOF using the method presented in [1]**.

It's a school project realized at Université Paul Sabatier, Toulouse, 2016/2017, targeting *linux* and *macOS*


[1] Sungkil Lee, Elmar Eisemann, and Hans-Peter Seidel. 2009. Depth-of-field rendering with multiview synthesis. In ACM SIGGRAPH Asia 2009 papers (SIGGRAPH Asia '09). ACM, New York, NY, USA, Article 134 , 6 pages.

## 'Product' state

At the time of the project deadline, here is (roughly) what is working:

* Simple rendering (Cook-Torrance model) and camera (FPS mode).
* Roundtable mode.
* Layered rendering.
* Layers decomposed following the paper.
* Recomposition partially works since the scene is visible, but many improvements to the intersection operator are yet to be implemented in order to have a good-looking result.
* Effects are function of the real optics parameters.

Textured color and 'stable' model loading were added later (9822fe7 among others).

Here are some screenshots. **The significant screenshot is the first one (Fig.1)**. Both use 32 lens samples and 20 layers.

### The final result

![fig.1](management/readme-pics/fig1a.jpg "Fig. 1a")

![fig.1](management/readme-pics/fig1b.jpg "Fig. 1b")

### Just for fun

This is the same scene with same parameters, rendered with a very primitive intersection test (enabled in shaders/recompose.glsl.frag by not defining `DEPTH_RECONSTRUCTION`). **This is therefore not based on the paper** - or partially - and **doesn't simulate every effects**. While this is a losely crafted version, it looks good too so here it is for the pleasure of one's eyes.

![fig.2](management/readme-pics/fig2a.jpg "Fig. 2a")

![fig.2](management/readme-pics/fig2b.jpg "Fig. 2b")

## Dependencies

The system must support OpenGL 4.1. If you have lower core profile you may replace all `QOpenGLFunctions_4_1_Core` with the supported core profile.

The system needs to provide assimp, glm and Qt 5 (providing QtGui module). It works well on linux and macOS needs some cmake options. Installing macOS libs was done using `brew` on our tests machines.

Furthermore, the Google Test suit is required in order to do tests, *but these are (for now) disabled*, because they actually do nothing. So for now you can be confident in compiling. Anyway, it's available on ubuntu (`gtest` and probably `libgtest-dev`) and arch (`community/gtest`).

## Compiling

    # Linux
    mkdir build
    cd build
    cmake ..
    make
    
    # macOS
    mkdir build
    cd build
    cmake .. -DCMAKE_PREFIX_PATH=<qt folder>   # for example /usr/local/Cellar/qt5/5.8.0
    make

    # Documentation
    make doxygen
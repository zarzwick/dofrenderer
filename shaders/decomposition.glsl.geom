#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

uniform int layerCount;

uniform vec3 lensPosition;
uniform float leftFactor;
uniform float nearDistance;


in VS_OUT {
    vec3 fragPos;
    vec3 fragNormal;
    vec2 fragUV;
} gs_in[];


out vec3 fragPos;
out vec3 fragNormal;
out vec2 fragUV;
out float near;

int getLayer()
{
    // We process the position of the triangle, so we will process the layer on its center.
    //vec3 barycenter = (gs_in[0].fragPos + gs_in[1].fragPos + gs_in[2].fragPos) / 3.0f;

    // According the paper, we must use "the maximum depth of the vertices." and not the barycenter.
    float d1 = length(lensPosition - gs_in[0].fragPos);
    float d2 = length(lensPosition - gs_in[1].fragPos);
    float d3 = length(lensPosition - gs_in[2].fragPos);

    // Distance between the triangle and the camera.
    float d = max(d1, max(d2, d3));
    d *= 1000.0f; // Convert to millimeter.

    // With this factor, the layer id will be accurate near the camera, and less far of the camera.
    float rightFactor = (1.0f / nearDistance) - (1.0f / d);

    // Equation 4.
    // Left Factor (CPU) = (K * df) / delta.
    // Right Factor (GPU) = (1 / dnear) - (1 / d).
    int layerID = int(leftFactor * rightFactor);

    return clamp(layerID, 0, layerCount - 1);
}

void main()
{
    int layer = getLayer();

    fragPos = gs_in[0].fragPos;
    fragNormal = gs_in[0].fragNormal;
    fragUV = gs_in[0].fragUV;
    gl_Position = gl_in[0].gl_Position;
    gl_Layer = layer;
    EmitVertex();

    fragPos = gs_in[1].fragPos;
    fragNormal = gs_in[1].fragNormal;
    fragUV = gs_in[1].fragUV;
    gl_Position = gl_in[1].gl_Position;
    gl_Layer = layer;
    EmitVertex();

    fragPos = gs_in[2].fragPos;
    fragNormal = gs_in[2].fragNormal;
    fragUV = gs_in[2].fragUV;
    gl_Position = gl_in[2].gl_Position;
    gl_Layer = layer;
    EmitVertex();
    EndPrimitive();
    
    near = nearDistance;
}

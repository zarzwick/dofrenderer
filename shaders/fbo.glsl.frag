#version 330 core

in vec2 TexCoords;
out vec4 color;

uniform sampler2DArray screenTexture;
uniform bool isDepth;
uniform int layerToDisplay;


void main()
{     
    if( isDepth )
    {
        float z = texture(screenTexture, vec3(TexCoords, layerToDisplay) ).x;
        color = vec4(z, z, z, 1.0f);
    }
    else
    {
        color = texture( screenTexture, vec3(TexCoords, layerToDisplay) );
    }
}

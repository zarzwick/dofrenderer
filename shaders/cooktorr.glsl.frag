#version 330 core

#define NB_LIGHTS 3

// Define lights and material structures
struct Light {
    vec3  coords;
    vec3  col;
    float intensity;
};

struct Material {
    vec3  colDiffuse;
    vec3  colSpecular;
    float roughness;
    int   textured;
};


const float PI = 3.1415926535897932384626433832795;


in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragUV;

in float near;

out vec4 color;


uniform float far;

// Lights array
uniform Light lights[NB_LIGHTS];

// Camera
uniform vec3 camPos;

// Mesh material
uniform Material mat;
uniform sampler2D diffuseTex_0;


float distribution( in vec3 h, in float roughness );
float fresnel( in float cosTeta );
float gCookTorr( in vec3 i, in vec3 o, in vec3 h );
float gSmith( in vec3 h, in vec3 v, in vec3 l );

float roughness = mat.roughness;


void main()
{
    // Process Depth according to the optics for second pass purpose.
    float distanceCam = length(camPos - fragPos) * 1000.0f;
    gl_FragDepth = (distanceCam - near) / (far - near);

    // Render using the Cook-Torrance microfacets model.
    vec3 kd, ks = mat.colSpecular;

    if ( mat.textured != 0 )
        kd = texture(diffuseTex_0, fragUV).xyz;
    else
        kd = mat.colDiffuse;

    // Some constants
    const float diffuse = 0.7;
    const float specular = 1.0 - diffuse;
    
    color = vec4(0.f,0.f,0.f,1.f);
    
    vec3 v = normalize(camPos - fragPos);

    for (int i = 0; i < NB_LIGHTS; ++ i)
    {
        // Some cool vectors
        vec3 l = normalize(lights[i].coords - fragPos);
        vec3 h = (l + v) / length(l + v);

        float F = fresnel( dot(l, fragNormal) );
        float D = distribution( h, roughness );
        float G = gSmith(h, v, l);

        // The rendering
        float specularFactor = (F * D * G) / ( 4 * max( dot(l,h), 0.f ) * max( dot(v,h), 0.f ) );
        color += vec4( ((diffuse * kd / PI) + (specularFactor * ks)) * max(dot(l, fragNormal), 0.0) , 0.f );
    }
}


// BRDF (Beckmann)
float distribution( in vec3 h, in float a )
{
    // Raise to the square (we only need this one)
    a = pow(a, 2);

    float nDotH = pow( dot( fragNormal, h ), 2 );

    // Compute the Gaussian-like part
    float A = exp( (nDotH - 1) / (a * nDotH) );
    float B = PI * a * pow(nDotH, 2);

    return A / B;
}


// Fresnel coefficient (Schlick's approx.)
float fresnel( float cosTeta )
{
    // Compute initial term (reflectance at normal incidance)
    float f0 = 0; //pow((1.000-1.680)/(1.000+1.680), 2);
    
    // Return Shlick's formula
    return f0 + (1-f0) * pow((1 - cosTeta), 5);
}


// Walter's associated approximation
float g1Walter( in vec3 v, in vec3 h )
{
    vec3 n = fragNormal;

    if ( dot(v,h) / dot(v,n) <= 0 )
        return 0.0;

    float a = 1 / (roughness * tan(acos(dot(n,v))));

    if ( a >= 1.6 )
        return 1.0;

    else
        return a*(3.535 + 2.181*a) / (1 + a*(2.276 + 2.577*a));
}


// Smith's shadowing function
float gSmith( in vec3 h, in vec3 v, in vec3 l )
{
    return g1Walter(v, h) * g1Walter(l, h);
}


// [1] http://simonstechblog.blogspot.fr/2011/12/microfacet-brdf.html

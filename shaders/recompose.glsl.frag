#version 330 core

#define MAX_REFINE_ITERATIONS 4 // 3 is said usually ok for convergence.
#define MAX_BINSEARCH_ITERATIONS 20 // Arbitrary value for now.

#define EPSILON 0.01f

#define DEPTH_RECONSTRUCTION


in vec2 fragUV;
out vec4 color;


/** Viewport width, height and film size, mandatory to compute offset. */
uniform uint w;
uniform uint h;
uniform vec2 pixmm; /* Pixel definition, e.g pix/mm. */

/** Focal and focal distance (mm) */
uniform float df;
uniform float focal;

/** Frustum limits */
uniform float dNear;
uniform float dFar;

/** Lens radius (E in [2]) */
uniform float lensRadius;

/** Lens samples */
#define POISSON_SAMPLES
#define NB_SAMPLES_MAX 32
uniform int NB_SAMPLES;
#ifdef POISSON_SAMPLES
uniform vec2 lensSamples[NB_SAMPLES_MAX];
#else
vec2 lensSamples[NB_SAMPLES];
#endif
float contribFactor = 1.f / NB_SAMPLES;

/** Number of layers available */
uniform int NB_LAYERS;

/** Distances (lower bound) for each layer */
#define NB_LAYERS_MAX 32  // WARNING Value from FrameBufferObject.hpp
uniform float layerDistance[NB_LAYERS_MAX];

/** Layers containing the scene depth and colour representation */
uniform sampler2DArray layersColor;
uniform sampler2DArray layersDepth;



/**
 * Compute a point offset according to the lens sample and dfocal.
 * @param df The focal distance (not the focal length).
 * @param d The actual distance.
 * @param E The lens radius.
 * @param v The sample offset (center being (0,0), in [-1,1]).
 */
vec2 offsetToPoint( float df, float d, float E, vec2 v );

/**
 * Return the depth value relative to camera.
 */
float realDepth( float bufferDepth );

/**
 * Method of False Position, tells if a surface is hit and where.
 * return .xy the pixel to fetch. .z is < 0 if no contact within the layer.
 */
vec3 contact( int lowerLayerIndex, vec2 pl, vec2 pu, float dl, float du );

/**
 * Intersection between two points. The precondition required for this function to work
 * is that lines are coplanar and non-parallel.
 * @param p[1-2] Points of the first line.
 * @param p[3-4] Points of the second line.
 * @return The point in 3D space closer to both lines.
 */
vec3 intersection( vec3 p1, vec3 p2, vec3 p3, vec3 p4 );

/**
 * Very specific implementation of a binary search.
 * It takes two 3D points and a layer index, and search the point where the segment [ab]
 * intersects with the surface in the layer. This function is to be called by contact( ... ),
 * as a precondition of this function is that the segment is guaranteed to intersect.
 * @param [a-b] The points defining the 3D segment.
 * @param layerIdx The layer's index.
 * @return The point in 3D space where the segment and surface intersect.
 */
vec3 binarySearch( vec3 a, vec3 b, int layerIdx );



void main()
{
    // Goal of this shader is to find each pixel's colors.
    // For each pixel (the fragments here), iteratively compute all the lens rays influence.
    // Influence of one ray is found by iteratively move along this ray until a surface is hit.
    color = vec4( 0.f, 0.f, 0.f, 1.f );

    float uf = (focal*df)/(df-focal);

    vec3 contactPoint;

    // Compute color contribution for each sample
    for ( int s = 0; s < NB_SAMPLES; ++ s )
    {
        // Sample offset ([-1,1]) on the lens.
        vec2 v = lensSamples[s];

        int l = 0;
        bool hit = false;
        bool valid = true;
        float dl, du;


        // March alongside the ray, by steps, in NB_LAYERS-1 first layers.
        while ( (valid) && (! hit) && (l < NB_LAYERS - 1) )
        {
            dl = layerDistance[l];
            du = layerDistance[l+1];

            // 1. Evaluate the ray segment's in and out positions (with offset).
            vec2 offsetLow = offsetToPoint( df, dl, lensRadius, v )/vec2(w,h);
            vec2 offsetUp = offsetToPoint( df, du, lensRadius, v )/vec2(w,h);

            // 2. Find the projection of pf alongside the center ray.
            vec2 pl = fragUV + offsetLow;
            vec2 pu = fragUV + offsetUp;

            // 3. Discard if out of layer texture.
            valid = pl.x <= 1.f && pl.x >= 0.f && pl.y <= 1.f && pl.y >= 0.f;

            // 4. Determine if the ray segment intersect with the surface.
            // [1: paper's technique | 0: simple test technique]
#ifdef DEPTH_RECONSTRUCTION
            contactPoint = contact( l, pl, pu, dl, du );
            hit = contactPoint.z >= 0.f;
#else
            contactPoint = vec3(pl, dl);
            hit = texture(layersDepth, vec3(pl, l)).x < 1.f;
#endif

            ++ l;
        }

        // 4. Just discard the ray if not valid (and dismiss any contribution).
        if ( ! valid )
        {
            //color.z += contribFactor;
            continue;
        }

        // 5. A contact was found before the last layer
        if ( hit )
        {
            // Compute color contribution using the nearest contact layer's bound.
            if ( contactPoint.z > (du+dl)*0.5 )
                color += texture( layersColor, vec3(contactPoint.xy, l) ) * contribFactor;
            else
                color += texture( layersColor, vec3(contactPoint.xy, l-1) ) * contribFactor;
        }
        // 6. No contact, the color contribution then comes from the last ("infinite") layer.
        else
        {
            vec2 offset = offsetToPoint( df, du, lensRadius, v )/vec2(w, h);
            contactPoint = vec3( fragUV + offset, NB_LAYERS );
            color += texture( layersColor, vec3(contactPoint.xy, NB_LAYERS-1) ) * contribFactor;
        }
    }

    // The color is now computed !
}


vec2 offsetToPoint( float df, float d, float E, vec2 v )
{
    // This returns a value in pixels, be very aware if you use UV : divide by (w,h) !
    return E * ((df - d) / d) * v * pixmm;
}


float realDepth( float bufferDepth )
{
    return (2.f * dNear * dFar) / (dFar + dNear - bufferDepth*(dFar-dNear)) ;
}


vec3 contact( int layerIdx, vec2 pl, vec2 pu, float dl, float du )
{
    // This function find the coordinates (UV space) of the point where a ray defined by its
    // two entry points pu (layer N+1) and pl (layer N) hit a surface, surface reconstructed
    // from the depth layer in this interval.
    // If a surface is hit, z component will contain 1, otherwise it will contain 0.
    // x and y components are the contact points coords.

    vec3 contact = vec3( -1.f );
    bool hit = false;
    int iterations = 0;

    vec2 a = pl;
    vec2 b = pu;

    // a equals b implies the segment is perpendicular. It's way more simple.
    if ( a == b )
    {
        float di = texture( layersDepth, vec3(a, layerIdx) ).x;

        if ( di < 1.f )
            return vec3( a, realDepth(di) );
        else
            return vec3( -1.f );
    }

    while ( (! hit) && (iterations < MAX_REFINE_ITERATIONS) )
    {
        // Fetch a and b depth.
        float da = texture( layersDepth, vec3(a, layerIdx) ).x;
        float db = texture( layersDepth, vec3(b, layerIdx) ).x;

        // Handle empty Z-depth (da or db = 1)
        if ( da == 1.f || db == 1.f )
            return vec3( 0.f, 0.f, -1.f );

        da = realDepth( da );
        db = realDepth( db );

        da = realDepth( da );
        db = realDepth( db );

        // Compute the intersection between the ray segment and the linear interpolation
        vec3 i = intersection( vec3(a, dl), vec3(b, du), vec3(a, da), vec3(b, db) );
        float di = realDepth( texture(layersDepth, vec3(i.xy, layerIdx)).x );

        //if ( di < i.z )
        if ( true )
        {
            // There IS an intersection, use binary search to find out.
            contact = binarySearch( vec3(a, dl), i, layerIdx );
            hit = true;
        }
        else
        {
            // No intersection can be deduced (but no proof there is not).
            // As such, the farther subsegment of the ray is tested.
            a  = i.xy;
            dl = i.z;
        }

        ++ iterations;
    }

    // Debug.
    if (iterations == MAX_REFINE_ITERATIONS)
        color.y += contribFactor;

    return contact;
}


vec3 intersection( vec3 p1, vec3 p2, vec3 p3, vec3 p4 )
{
    // Find 3D intersection between two coplanar lines defined by (p1, p2) and (p3, p4).

    // NOTE: If the two lines aren't actually coplanar, the result will still be "valid"
    // (but not exact) in that it will represent the point closer to any point of the two lines.

    // WARNING: This function just doesn't handle parallel lines. It could, at the cost of a
    // procedure to handle it in the calling function, and a fourth component to tell it.

    // Method from Numerical Recipe, 3rd Edition, 21.4.2.

    vec3 u = p4 - p3, v = p2 - p1;
    vec3 c = p1 - p3;
    vec3 uxv = cross(u, v);

    float denom = 1.f / pow(length(uxv), 2);

    // Compute s and t such as p1 + sv ~ p3 + tu.

    // s.
    mat3 mat = mat3(c,u,uxv);
    float s = determinant(mat) * denom;

    // t.
    mat[1] = v;
    float t = determinant(mat) * denom;

    vec3  a = p1 + s*v;
    vec3  b = p3 + t*u;
    vec3  i = (a+b) * 0.5f;

    return i;
}


struct Line
{
    vec3 o; // Origin.
    vec3 v; // Direction vector.
};


vec3 binarySearch( vec3 a, vec3 b, int layerIdx )
{
    // This function implements do a 'binary search' with the secant method, for it doesn't
    // need the function to be monotonic. The latter function is defined by the depth texture
    // so the intersection is the intersection between the surface and the ray segment, just
    // like the calling function contact().

    // To find the intersection, we look for the root of the function f = line(a,b) - tex().

    vec2 i;
    float zi;

    // 1. Create the line defined by a and b.
    Line ab;
    ab.o = a;
    ab.v = b - a;

    // 2. Find the z of points a and b projected on f.
    vec2 n = a.xy;
    vec2 m = b.xy;
    float dn = realDepth( texture(layersDepth, vec3(n, layerIdx)).x );
    float dm = realDepth( texture(layersDepth, vec3(m, layerIdx)).x );
    //         |<-        The [ab] line part      ->| |<- The depth part ->|
    float zn = ab.o.z + ((n.x-ab.o.x)/ab.v.x)*ab.v.z -    dn;
    float zm = ab.o.z + ((m.x-ab.o.x)/ab.v.x)*ab.v.z -    dm;

    // 3. Loop until convergence.
    bool convergence = false;
    int iterations = 0;
    while ( (! convergence) && (iterations < MAX_BINSEARCH_ITERATIONS) )
    {

        // 4. Find the root of the line [n,m].
        Line nm;
        nm.o = vec3(n, zn);
        nm.v = vec3(m, zm) - nm.o;

        float t = - nm.o.z / nm.v.z;

        i = vec2( nm.o.x + t*nm.v.x, nm.o.y + t*nm.v.y );
        zi = nm.o.z + ((i.x-ab.o.x)/ab.v.x) * nm.v.z;

        // 5. Test for convergence.
        if ( abs(zi) < EPSILON )
        {
            convergence = true;
        }
        else
        {
            n = m;
            zn = zm;
            m = i;
            zm = zi;
        }

        ++ iterations;
    }

    // NOTE The z calculations are done using the parametric representation of lines:
    // x(t) = o.x + t*v.x
    // ...

    return vec3(i, zi);
}


#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 normalTransform;

out VS_OUT {
    vec3 fragPos;
    vec3 fragNormal;
    vec2 fragUV;
} gs_out;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0);

    gs_out.fragPos = vec3( model * vec4(position, 1.0) );
    gs_out.fragNormal = vec3( normalTransform * vec4(normal, 0.0) );
    gs_out.fragUV = uv;
}


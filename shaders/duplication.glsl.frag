#version 330 core

in vec2 TexCoords;
out vec4 color;

uniform sampler2DArray fboColors;
uniform sampler2DArray fboDepths;
uniform sampler2DArray duplicationColors;
uniform sampler2DArray duplicationDepths;

uniform int currentLayer;
uniform int layerCount;
    
uniform float leftFactor;
uniform float nearDistance;
uniform float farDistance;

uniform int debugColor;

float getDistance( float depth )
{    
  return ( depth * ( farDistance - nearDistance ) + nearDistance );
}

int getLayer(float d)
{
    // With this factor, the layer id will be accurate near the camera, and less far of the camera.
    float rightFactor = (1.0f / nearDistance) - (1.0f / d);

    // Equation 4.
    // Left Factor (CPU) = (K * df) / delta.
    // Right Factor (GPU) = (1 / dnear) - (1 / d).
    int layerID = int(leftFactor * rightFactor);

    return clamp(layerID, 0, layerCount - 1);
}

float getStartLayer( int layer )
{
    float bot = ( -float(layer) / leftFactor ) + ( 1.0f / nearDistance );
    
    float d = 1.0f / bot;
    
    return d;
}

bool isOverlaping( int layer, float d )
{
    if( layer != currentLayer - 1 )
        return false;

    float startCurrentLayer = getStartLayer( currentLayer );
    float startPreviousLayer = getStartLayer( layer );
    
    float totalDistance = startCurrentLayer - startPreviousLayer;
    float relativeDistance = d - startPreviousLayer;
    
    if( relativeDistance >= (totalDistance * 0.9f) && relativeDistance < startCurrentLayer )
        return true;
    
    return false;
}

void processOverlap()
{
    if( currentLayer <= 0 )
        return;

    int previousLayer = currentLayer - 1;
    
    float fragDepth = texture( duplicationDepths, vec3(TexCoords, previousLayer) ).x;
    float fragDistance = getDistance( fragDepth );
    
    if( isOverlaping( previousLayer, fragDistance ) )
    {
        gl_FragDepth = fragDepth;
        color = debugColor == 1 ? vec4(1.0f, 0.0f, 0.0f, 1.0f) : texture( duplicationColors, vec3(TexCoords, previousLayer) );
    }
}

void duplicateBigTriangles()
{
    float fragDepth = 0.0f;
    float fragDistance = 0.0f;
    
    for( int layer = currentLayer + 1; layer < layerCount; layer++ )
    {
        fragDepth = texture( fboDepths, vec3(TexCoords, layer) ).x;
        fragDistance = getDistance( fragDepth );
        
        if( getLayer( fragDistance ) == currentLayer && fragDepth < gl_FragDepth )
        {
            gl_FragDepth = fragDepth;
            color = debugColor == 1 ? vec4(0.0f, 0.0f, 1.0f, 1.0f) : texture( fboColors, vec3(TexCoords, layer) );
            //color = texture( fboColors, vec3(TexCoords, layer) ) * (10.0f * float( getLayer( fragDistance ) ) / float(layerCount - 1));            
        }
    }
}

void main()
{
    gl_FragDepth = texture( fboDepths, vec3(TexCoords, currentLayer) ).x;
    color = debugColor == 1 ? vec4(0.0f, 1.0f, 0.0f, 1.0f) : texture( fboColors, vec3(TexCoords, currentLayer) ); 
    
    processOverlap();
    
    duplicateBigTriangles();
}

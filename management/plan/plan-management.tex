\documentclass[a4paper, 12pt, french, notitlepage]{article}

\usepackage[margin=2cm]{geometry}
\usepackage[hidelinks]{hyperref}
\usepackage[bottom]{footmisc}
\usepackage{enumitem}
\usepackage{fontspec}
\usepackage{xunicode}
\usepackage{graphicx}
\usepackage{pgfgantt}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{float}
\usepackage{babel}

\usepackage{tikz}
\usepackage{tikz-qtree}

\usetikzlibrary{trees}

\setmainfont{Noto Serif}
\setlength{\parskip}{1.1em}
\renewcommand{\baselinestretch}{1.2}

\newcommand{\version}{10}


% Where the document begins ! ---------------------------------------------------------------------
\begin{document}


% Title
\title{Plan Management v\version}
\author{Hugo Rens, Rémi Maigné et Nie Yi (équipe RHYME),\\ pour le compte de Mathias Paulin}

\maketitle

\newpage

\tableofcontents

\newpage

\paragraph{Versions}
L'historique des changements est disponible dans
plan-changelog.txt\textsuperscript{\cite{changelog}}.

\noindent
v1 : établie le 13 janvier 2017.\\
v2 : établie le 27 janvier 2017.\\
v3 : établie le 8 février 2017.\\
v4 : établie le 23 février 2017.\\
v5 : établie le 1 mars 2017.\\
v6 : établie le 8 mars 2017.\\
v7 : établie le 15 mars 2017.\\
v8 : établie le 22 mars 2017.\\
v9 : établie le 10 avril 2017.\\
v\version : établie le \today{}.\\


{\itshape
\paragraph{Intégration}
Ce document est le plan de management correspondant au projet réalisé dans le cadre de l'UE Projet du M1 informatique de l'université Paul Sabatier, année 2016/2017. Le sujet s'intitule \textbf{Photographie numérique : prise de vue réaliste en synthèse d'image.} et consiste à implémenter un démonstrateur pour la technique de profondeur de champs présentée dans l'article de recherche "Depth-of-Field Rendering with Multiview Synthesis"\footnote{Depth-of-Field Rendering with Multiview Synthesis, by Sungkil Le, Elmar Eisemann and Hans-Peter Seidel, ACM Transactions on Graphics, Vol. 28, No. 5, Article 134, Publication date: December 2009}.}


% Sections, finally.


\section{Parties prenantes}

Le maître d'ouvrage est Mathias Paulin, enseignant-chercheur à l'IRIT\footnote{Institut de Recherche en Informatique de Toulouse}.


Les maîtres d'œuvre sont les constituants de l'équipe, Hugo Rens, Rémi Maigné et Nie Yi, qui endossent respectivement les rôles de chef de projet, responsable de documentation et responsable qualité\footnote{Ces rôles ne sont pas fixés dans le temps, et après concertation avec le client sont susceptibles d'êtres intervertis si la situation l'indique.} (ce dernier est celui interne à l'équipe).


Les responsables de l'UE Projet sont des parties prenantes tierces, qui requièrent le respect de certaines contraintes supplémentaires vis-à-vis de la qualité logicielle, la qualité de l'organisation et des délais. En tant que tel, ils sont les responsables qualité (et service juridique) principaux.


\begin{center}
\begin{tabular}{| l | l | l | l | l | l |}
\hline
 & Client & Resp. UE Projet & Chef & Resp. doc. & Resp. qualité\\
\hline
Documentation & I, C & I, C & R & A, R & R\\
\hline
Livrables papier & A, I, C & I, C & R & R & R\\
\footnotesize{plan de management} & & & & & \\
\footnotesize{spécification} & & & & & \\
\footnotesize{conventions de code} & & & & & \\
\footnotesize{changelogs} & & & & & \\
\hline
Livrables source & A, I, C & & R & R & R\\
\hline
Échéances projet & I, C & A & R & R & R\\
\hline
Développement projet & I, C &  & R, A & R, C & R, C\\
\hline
Spécifications projet & I, C &  & C & C & R, A\\
\hline
\end{tabular}
\captionof{table}{Matrice RACI}
\end{center}


\section{Contenu}

\subsection{Exigences fonctionnelles}


Les exigences du client sont telles que le rendu soit un démonstrateur 3D temps-réel.
Ce démonstrateur doit permettre de bien \textbf{distinguer l'effet de profondeur de champ (fig. \ref{bokeh}) produit par la technique décrite} (exigence f1) sur une ou plusieurs scènes 3D significatives (exigence f2).


En dehors de la profondeur de champ, le moteur supportera le modèle Cook-Torrance pour rendre les modèles (exigence f3).


Les différents paramètres physiques simulés doivent pouvoir être modifiés dynamiquement (exigence f4). On compte parmi ces paramètres:
\begin{itemize}
  \item la distance focale
  \item l'ouverture de l'objectif
  \item la taille (résolution) du capteur
\end{itemize}

Le simulateur pourra, optionnellement, simuler des effets de Bokeh, qui apparaissent en photographie sous la forme de formes géométriques plus ou moins floues, et qui sont liées à la profondeur de champ.


\begin{figure}[h]
\begin{center}
\includegraphics[width=0.5\textwidth]{bokeh.jpg}
\caption{\textbf{Exemple d'image} présentant la différence entre les zones nettes (1) et floues (2). On aperçoit également un effet de bokeh en (3).}
\label{bokeh}
\end{center}
\end{figure}


Les exigences métier sont représentées sous forme d'arborescence dans la Work Breakdown Structure (fig. \ref{wbs}). Une estimation des différentes charges (en heure/homme) est fournie dans \ref{charges}.

\begin{figure}
\begin{center}
\begin{tikzpicture}[level distance=3em, edge from parent fork down,
  every node/.style = {shape=rectangle, draw}]]
  \Tree
  [.DOFRenderer
    [.Interface
      [.Rendu Global Par\ couche ]
      [.Contrôles
        [.Appareil\ physique Ouverture Focale ]
        [.Précision Variation\ du\ CdC ]
      ] ]
    [.Moteur Décomposition Reconstruction ]
  ]
\end{tikzpicture}
\caption{Work Breakdown Structure}
\label{wbs}
\end{center}
\end{figure}


\newpage
\subsection{Exigences non fonctionnelles}

\paragraph{}
Ce simulateur doit être fourni sous la forme de code C++ compilable avec un compilateur supportant C++11, ainsi que les librairies Qt\footnote{\url{https://www.qt.io/}} et Assimp\footnote{\url{http://www.assimp.org/main\_viewer.html}} et les drivers graphiques appropriés (exigence nf1).

Le démonstrateur doit utiliser l'API graphique OpenGL (exigence nf2).

Les plateformes ciblées sont \textbf{Linux} et \textbf{macOS} (exigence nf3).


\section{Communications}

\paragraph{Client - Équipe}
Les communications avec le client se font par email et lors de réunions qui ont lieu toutes les deux semaines. Lors de ces réunions seront discutés l'avancement du projet, son déroulement ainsi que des détails techniques.

Le client à également accès au dépôt git du projet, ce qui assure automatiquement la transmission des diverses avancées du projet et des documents produits.

\paragraph{Équipe - Équipe}
En interne, l'équipe utilise surtout skype, qui offre une capacité à être réactifs et à communiquer en temps réel; elle se réunit quand c'est nécessaire.


\paragraph{Resp. UE - Équipe}
Les communication avec les responsables de l'UE sont également réalisées par mails, ainsi que via les réunions aménagées.


\section{Risques}

Une \textbf{incompréhension bloquante de l'article source}, qui devra être résolue en collaboration avec le client, peut potentiellement freiner ou retarder l'achèvement du projet. Cette compréhension est essentielle au reste du projet.


Le projet représentant un certain nombre de tâche, le risque qu'une de ces tâches ne soit pas réalisées existe. L'étape de spécification citée ci-dessus est primordiale et un prérequis pour toutes les autres (excepté le moteur en lui-même).


Si le moteur ne peut pas \textbf{charger de modèles}, une scène par défaut peut être fournie en dur en tant que démonstration pour la profondeur de champ.


Si la \textbf{décomposition} n'est pas fonctionnelle, des images de test peuvent également être créées pour démontrer le succès de la reconstruction de l'image.


Si la \textbf{reconstruction} échoue, on pourra tout de même visualiser les différentes couches.


L'\textbf{indisponibilité d'un des membres, temporaire ou définitive} qui causerait également un net ralentissement, et qui devra être résolue en interne. Une consultation avec les diverses parties prenantes peut être souhaitable cependant.


Voici un tableau présentant une estimation simplifiée des probabilités de chaque type de risque et de leur dangerosité. Aucune échelle réelle n'est définie mais chaque risque est évalué par rapport aux autres.


\begin{center}
\begin{tabular}{|c|c|c|}
\hline
 & Risque & Impact\\
\hline
Incompréhension & Élevé & Critique\\
\hline
Chargement de scène & Très faible & Minime\\
\hline
Décomposition en couches & Faible & Important\\
\hline
Recomposition & Faible & Important\\
\hline
Problème humain & Très faible & Important\\
\hline
\end{tabular}
\captionof{table}{Tableau d'estimation des risques}
\end{center}


\section{Délais}

\subsection{Estimations horaires}

% Stories techniques

\label{charges}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline

Rendu de base & t1 & \\
\hspace*{0.5cm}Chargement de modèle & t11 & 20h\\
\hspace*{0.5cm}Matériau & t12 & 8h\\
\hspace*{0.5cm}Rendu OpenGL & t13 & 4h\\
\hspace*{0.5cm}Caméra & t14 & 6h\\
\hline

Paramètres physiques & t2 & \\
\hspace*{0.5cm}Unités (mm, px) & t21 & 2h\\
\hspace*{0.5cm}Paramètres physiques (et interface) & t22 & 20h\\
    \hspace*{1cm} Description dans le code & t221 & 12h\\
    \hspace*{1cm} UI & t222 & 3h\\
    \hspace*{1cm} Interfaçage UI - Code & t223 & 5h\\
\hline

Effet de profondeur & t3 & \\

\hspace*{0.5cm}Décomposition & t31 & \\
    \hspace*{1cm}Nombre de layers & t311 & 4h\\
    \hspace*{1cm}Cercle de confusion & t312 & 6h\\
    \hspace*{1cm}Rendu dans un framebuffer séparé & t313 & 12h\\
        \hspace*{1.5cm}Implémentation naïve (pair/impair) & t3131 & 8h\\
        \hspace*{1.5cm}Adaptation / Dynamisation & t3132 & 10h\\
            \hspace*{2cm}Rendu & t31321 & 8h\\
            \hspace*{2cm}UI & t31322 & 2h\\
    \hspace*{1cm}Séparation en couches & t314 & 6h\\
    \hspace*{1cm}Propagation des triangles & t315 & 20h\\ % TODO Euh...

\hspace*{0.5cm}Recomposition & t32 & \\
    \hspace*{1cm}Méthode des fausses positions & t321 & 20h\\
        \hspace*{1.5cm}Implémentation naïve & t3211 & 8h\\
        \hspace*{1.5cm}Étude des cas particuliers & t3212 & 8h\\
        \hspace*{1.5cm}Correctifs cas particuliers & t3213 & 4h\\
    \hspace*{1cm}Approximation du cône & t322 & 20h\\
        \hspace*{1.5cm}Création des mipmaps & t3221 & 3h\\
        \hspace*{1.5cm}Étude et implémentation & t3222 & 17h\\ % TODO À définir
    \hspace*{1cm}Compositing & t323 & 5h\\
\hline

Infrastructure de démonstration & t4 & \\
\hspace*{0.5cm}Trajectoire de la caméra & t41 & x h\\
\hspace*{0.5cm}Scènes significatives & t42 & x h\\
\hspace*{0.5cm}Interface de démonstration & t43 & x h\\

& & \\

\end{tabular}
%\captionof{table}{Liste des tâches de développement}

\begin{tabular}{|l|c|c|}
& & \\
\hline
Amélioration expérience utilisateur & t5 &\\
\hspace*{0.5cm}Caméra & t51 &\\
    \hspace*{1cm} Trackpoint & t511 & x h\\
    \hspace*{1cm} Positionnement automatique de la caméra & t512 & x h\\
\hspace*{0.5cm}Correction des normales (rendu) & t52 & 1h\\
\hspace*{0.5cm}Rechargement de scène & t53 & 2h\\
\hspace*{0.5cm}Modification de scène & t54 &\\
    \hspace*{1cm} Séléction d'objet & t541 & 1h\\
    \hspace*{1cm} Modification de paramètres & t542 & 1h\\
\hspace*{0.5cm}Interface d'aide & t55 & \\
    \hspace*{1cm} Interface utilisateur & t551 & 2h\\
    \hspace*{1cm} Documentation integrée (F1) & t552 & 1h\\
\hline
\end{tabular}
\captionof{table}{Liste des tâches de développement}

\end{center}

Les tâches groupées sous la dénomination "Divers" sont des tâches courtes, qui apparaissent nécessaire à un point du projet, mais pas vraiment prioritaires ni urgentes. Ainsi lorsqu'il nous reste des créneaux ou que l'on ne peut réaliser une tâche en une itération par manque de temps assignable, l'un de nous peut prendre une ou plusieurs de ces tâches.


\subsection{Itérations}

Cette sous-section présente les plans d'itérations \textbf{prévisionnels}. Des résumés à posteriori de ces itérations sont intégrés après chacun des plannings, et \textbf{les rapports complets sont disponibles} (voire section qualité) dans \texttt{management/iterations/}.

Les itérations sont plannifiées en fonction de notre connaissance courante du projet, et par dépendances du code.

Voici donc les parties effectuées à l'itération 3, qui a commencé après le rapport v3, et se terminant la semaine du 20 février\footnote{On pourra se referrer aux compte-rendus des itérations dans \texttt{itérations/} pour plus de détails}.

\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.7cm,
     y unit chart = 1.1cm
    ]{1}{12}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 3}}{10} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,10}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{1}{12}\\
  \ganttbar{Geometry shader (décomposition)}{1}{6}\\
  \ganttbar{Rendu dans framebuffer séparé}{7}{10}\\
  \ganttbar{Visualisation des framebuffers}{11}{12} \\
  \ganttgroup{Hugo}{1}{11}\\
  \ganttbar{Chargement de modèles}{1}{2}\\
  \ganttbar{Paramètres des matériaux}{3}{11}\\
  \ganttgroup{Yi}{1}{10}\\
  \ganttbar{Description param. physiques (code)}{1}{10}\\
\end{ganttchart}

Cette itération s'est suivie d'une réunion avec le client où le moteur de rendu de base lui a été présenté. Le résultat correspondait à la direction attendue.


\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.7cm,
     y unit chart = 1.1cm
    ]{1}{12}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 4} (23/02 au 08/03)}{10} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,10}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{1}{11}\\
  \ganttbar{Adaptation (framebuffer)}{1}{8}\\
  \ganttbar{UI (framebuffer)}{9}{11} \\
  \ganttgroup{Hugo}{1}{11}\\
  \ganttbar{Param. des matériaux}{1}{2}\\
  \ganttbar{Fausses positions (méth. naïve)}{3}{11}\\
  \ganttgroup{Yi}{1}{6}\\
  \ganttbar{Paramètres optiques (UI)}{1}{6}\\
\end{ganttchart}

Nous avons constaté à l'itération précédente que \textbf{le temps nous a manqué pour effectuer nos tâches}, avec pour cause des éléments extérieurs au projet.
Il en découle donc que les tâches assignées n'ont pas toutes été atteintes, en particulier celle concernant l'implémentation naïve de la méthode des fausses positions.

Sachant que la situation a de très fortes chances de rester identique à l'itération 5, nous prévoyons, en accord avec le client, une charge de travail inférieure aux 10h/développeur habituelles.

\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.7cm,
     y unit chart = 1.1cm
    ]{1}{12}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 5} (08/03 au 22/03)}{10} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,10}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{2}{4}\\
  \ganttbar{Amélioration UX}{2}{4}\\
  \ganttgroup{Hugo}{2}{4}\\
  \ganttbar{Amélioration UX}{2}{4}\\
  \ganttgroup{Yi}{2}{4}\\
  \ganttbar{Amélioration UX}{2}{4}\\
\end{ganttchart}

Lors de cette itération, le client a explicité des cas d'utilisation supplémentaires ainsi que des correctifs souhaitables, retrouvables sous l'intitulé "Divers". Ces tâches relativement courtes seront pour certaines effectuées lors de cette itération, dont l'effort maximum est réduit.

L'itération 6 souffre des mêmes problèmes d'horaires. Nous ferons donc des tâches de la catégorie "Amélioration Interface Utilisateur" pour avancer un peu et obtenir un résultat.

\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.7cm,
     y unit chart = 1.1cm
    ]{1}{12}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 6} (22/03 au 05/04)}{10} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,10}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{2}{3}\\
  \ganttbar{Interface d'aide (UI)}{2}{3}\\
  \ganttgroup{Hugo}{2}{3}\\
  \ganttbar{Modification de scène}{2}{3}\\
  \ganttgroup{Yi}{2}{3}\\
  \ganttbar{Rechargement de scène}{2}{3}\\
\end{ganttchart}

Comme prévu, l'itération 6 a été très pauvre en temps allouable, mais les tâches ont été réalisées. Le rechargement de scène n'est pas apparu directement exactement à la fin de l'itération, mais la fonctionnalité été prête. Le dévelopeur Hugo a également pris plus de temps que prévu pour réaliser sa tâche. % Euh, faut-il réellement qu'on dise la deuxième phrase - qui parle du reset de scène ?
Des petites améliorations sont possibles, mais les changements sont trop fins pour justifier une tâche complète.

Le temps de développement disponible pour les développeurs revient à 10h em moyenne pour l'itération 7. Des fonctionnalités critiques peuvent être ébauchées et implémentées en "bêta".

\label{iteration}
\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.7cm,
     y unit chart = 1.1cm
    ]{1}{12}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 7} (06/04 au 19/04)}{10} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,10}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{2}{11}\\
  \ganttbar{Nombre de layers}{2}{5}\\
  \ganttbar{Cercle de confusion}{6}{11}\\
  \ganttgroup{Hugo}{2}{11}\\
  \ganttbar{Améliorations utilisateur (bugfix)}{2}{3}\\
  \ganttbar{Méthode des fausses positions (naïve)}{4}{11}\\
  \ganttgroup{Yi}{2}{11}\\
  \ganttbar{Trajectoire de la caméra}{2}{11}\\
\end{ganttchart}

L'itération 7 s'est déroulée plutôt normalement en terme de temps développeur. Les bases des fonctionnalités existent en majorité dans le code mais ne sont pas encore opérationnelles cependant. Pour cause, une partie non-négligeable du temps a aussi été consacrée à de l'administration de dépôt et à la correction de bugs mineurs.
De plus, nous avons activé et utilisé le gestionnaire d'issues de bitbucket (voir \ref{VCS}).

Une meilleure gestion de cette administration git, assurée par le chef de projet, gagnerait à être améliorée. Il serait également souhaitable d'avoir plus d'informations temps réel de la part de chaque développeur, avec des indications d'avancement (qu'on ait avancé ou non). Une utilisation plus régulière et systématique dudit système de versions serait une solution largement suffisante quand du code (même non fonctionnel) existe sur une branche.

La prochaine itération, l'itération 8, vise à compléter et figer les fonctionnalités précédentes. Certains développeurs peuvent allouer un temps plus important de l'ordre de la vingtaine d'heures pour cette itération.

\label{iteration}
\begin{ganttchart}
    [vgrid = {{dashed}, *1{draw = none}},
     x unit = 0.45cm,
     y unit chart = 1.1cm
    ]{1}{22}
  \gantttitle{}{1} \gantttitle{\textbf{Itération 8} (20/04 au 03/05)}{20} \gantttitle{}{1}\\
  \gantttitle{}{1} \gantttitlelist{1,...,20}{1} \gantttitle{}{1}\\
  \ganttgroup{Rémi}{2}{21}\\
  \ganttbar{Nombre de layers}{2}{3}\\
  \ganttbar{Cercle de confusion}{4}{8}\\
  \ganttbar{Séparation en courches}{9}{16}\\
  \ganttgroup{Hugo}{2}{21}\\
  \ganttbar{Améliorations util. (bugfix)}{2}{5}\\
  \ganttbar{Méth. fausses positions (naïve)}{6}{12}\\
  \ganttbar{Méth. fausses positions (cas)}{14}{21}\\
  \ganttgroup{Yi}{2}{21}\\
  \ganttbar{Trajectoire de la caméra}{2}{12}\\
  \ganttbar{Interface de démonstration}{13}{21}\\
\end{ganttchart}



\paragraph{Échéancier}
\begin{description}
  \item [15 jan.] Plan initial de Management
  \item [15/16 mai] Soutenance
  \item [12 mai] Recette avec le client
  %\item{} ...
\end{description}


\subsection{Effort moyen}

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}
\begin{axis}
[x tick label style={/pgf/number format/1000 sep=},ylabel=Effort moyen,enlargelimits=0.15,legend style={at={(0.5,-0.25)},anchor=north,legend columns=-1},ybar,bar width=12pt,]
\addplot coordinates{ (3,10) (4, 9) (5, 1) (6, 2) (7, 6) };
\addplot coordinates{ (3,8)  (4, 8) (5, 1) (6, 2) (7, 6) };
\addplot coordinates{ (3,10) (4, 5) (5, 1) (6, 4) (7, 10) };
% Method of the RACHE used for times of Rémi and Yi. Please correct if false.
\addplot[red,line legend,sharp plot,update limits=false] coordinates{(2,5.888) (10,5.888)};% node[above]; at (0,0) {Moyenne}; % This doesn't work.
\legend{Rémi,Yi,Hugo}
\end{axis}
\end{tikzpicture}
\end{center}
\caption{Diagramme de vélocité}
\end{figure}

\section{Qualité}

Il est présenté ici les technologies utilisées
\begin{itemize}
  \item C++ 11, qui est un standard récent, éprouvé et disposant d'une excellente librairie standard sur toutes les plateformes cibles.
  \item Qt ≥ 5.4 avec le module QtGui. Qt est cross-platform, permet de créer des interfaces graphiques facilement et simplifie l'intégration d'une vue OpenGL, ce qui nous permet d'avoir rapidement un moteur de rendu basique fonctionnel. Qt5 en version communautaire est redistribué sous licence LGPLv3.
  \item LLVM Clang (3.x) et GCC (5.x ou 6.x) en compilateurs de test.
  \item Les OS de test sont une ArchLinux (driver mesa 17, kernel 4.9.6-1), une Ubuntu 16.04 LTS (driver nvidia propriétaire) et un macOS 10.6.8. % De mémoire, à vérifier
  \item Skype version 1.17 alpha.
  \item Git version 2.10 minimum.
  \item Assimp en version présente sur les distributions linux (3.3.1) et macOS (3.1.1 avec Homebrew). Assimp est sous licence MIT modifiée.
\end{itemize}


\paragraph{}
\label{VCS}
La gestion du code se fait via un \textbf{dépôt git}\textsuperscript{\cite{bitbucket}} pour permettre de revenir en arrière en cas de régression, gérer les versions et travailler plus facilement en collaboration, et des tests sont effectués sur tous les systèmes ciblés. Une documentation du code est génerable automatiquement via doxygen.

\subsection{Méthodologie}

Nous effectuerons des rendez-vous semi-hebdomadaires avec le client, et le dépôt périodique d'une version préliminaire du projet, qui devra compiler et s'exécuter. \textbf{Des comptes rendus de ces rendez-vous sont disponibles dans} \texttt{management/meetings/}\footnote{\url{https://bitbucket.org/zarzwick/dofrenderer/src/master/management/meetings/?at=master}}.

Notre méthode de développement sera basé sur la méthode \textbf{OpenUP}. Nous effectuerons des \textbf{incréments} hebdomadaires, afin de nous coordonner et de se consulter, de faire un inventaire de toutes les fonctionnalités ajoutées et à ajouter en complément du log git.

Chacune de nos itérations, durant 2 semaines et calée à partir du mercredi de la première semaine, vise à achever la production de plusieurs incréments (qui correspondent assez à la segmentation présentée dans la section Contenu). Des rapports sur itérations sont trouvables dans le répertoire \texttt{iterations/}\footnote{\url{https://bitbucket.org/zarzwick/dofrenderer/src/master/management/iterations/?at=master}}
du projet.

Chaque itération représente un effort théorique de 10h/homme (soit 30h pour l'équipe). Les heures sont matérialisées par les 1..10 des plan d'itération (section Délais).

Il est à noter que les deux premières itérations n'ont pas été effectuées en tant que telle, car cette période correspondait au démarrage du projet et à l'établissement même de ces itérations.

Ces deux premières itérations peuvent donc être assimilées à la phase d'inception du projet. Cependant, il est préférable de compter également ce temps dans les itérations, du code de base ayant été produit pendant cette période (correspondant au 'Rendu de base' du tableau des tâches \ref{charges}).

De plus, le projet est basé sur un article, dont la lecture et la compréhension sont en partie dépendantes de l'avancement du projet. Cette lecture n'est pas entièrement contenue dans la phase d'inception.


\paragraph{}
Nous suivrons également certaines conventions de code qui sont listées dans coding-conventions.txt\textsuperscript{\cite{codeconvs}}, ce qui permettra de garantir une consistance plus forte dans le code.


Le contenu des itérations est fait en fonction des exigences métier, par rapport à ce que le client souhaite voir apparaître en premier. En l'occurence, ces besoins n'ont pas réellement besoin d'être négociés car ils coincident exactement avec les dépendances fonctionnelles. \textbf{Cependant, le client peut parfaitement demander à prioriser telle ou telle tâche}.


\subsection{Tests}

\subsubsection{Tests de recette}

\paragraph{But des tests}
Des tests de recette ont déjà définis avec le client afin de montrer les capacités du démonstrateur.
Ce dernier pourra utiliser une scène générée procéduralement, ou une scène importée. Ces \textbf{tests fonctionnels} auront pour but de simuler une utilisation du logicielle présentant des fonctionnalités significatives.

Ces tests consisteront en une \textbf{démonstration visuelle automatique} (mouvement sans intervention de l'utilisateur) qui montrera les effets de profondeur sur une scène comportant des objets distants, ainsi que les effets d'effacement causés sur des objets proches\footnote{Tels que l'on peut voire sur des photos prises derrière des barreaux, où ces derniers apparaissent en légère transparence ou disparaissent.}.

\textbf{Le test devra également mettre en évidence les cas limites} (les pires cas).

\paragraph{Déroulement des tests}
Le logiciel disposera d'un bouton déclenchant
\begin{enumerate}
    \item la naviguation au travers de celle-ci en mode spectateur (exigence f1)
    \item le chargement d'une scène adéquate (exigence f2)
    \item la modification à la volée de certains paramètres (exigences f2, f4)
\end{enumerate}


\subsubsection{Tests de code}

La solution de tests google tests\footnote{\url{https://github.com/google/googletest}} est à l'étude pour être utilisée dans le projet en tant qu'outil d'automatisation des tests, et de contrôle de non-régression.

Il est important de noter qu'\textbf{aucun test n'est effectif actuellement}.


\section{Maîtrise}

Le projet n'utilise aucune librairie sous licence propriétaire. \textbf{Il n'a pas vocation à être commercialisé}.

Les techniques utilisées sont basées sur la méthode présentée dans un papier de recherche non-soumis à brevet.

Le projet est sous \textbf{licence BSD}, dont une copie est fournie à la racine du projet \footnote{\url{https://bitbucket.org/zarzwick/dofrenderer/src/master/LICENSE?at=master\&fileviewer=file-view-defaul}}. Cette license est compatible avec celles des librairies utilisées, et reste permissive. Le client n'a exprimé aucune contrainte forte sur ce point.

Les licences des librairies ne sont pas reproduites ici car non-inclues dans le projet. Elles sont cependant disponibles sur leurs sites respectifs.

\vfill{}
\begin{thebibliography}{3}

\bibitem{bitbucket} 
Dépôt bitbucket, \url{https://bitbucket.org/zarzwick/dofrenderer}
 
\bibitem{codeconvs}
Conventions de code, \url{https://bitbucket.org/zarzwick/dofrenderer/src/master/coding-conventions.txt?at=master\&fileviewer=file-view-defaul}

\bibitem{changelog}
Changelog, \url{https://bitbucket.org/zarzwick/dofrenderer/src/master/management/plan/plan-changelog.txt?at=master\&fileviewer=file-view-defaul}

\end{thebibliography}

\end{document} % ----------------------------------------------------------------------------------


% [1] https://en.wikibooks.org/wiki/LaTeX/Document_Structure#Document_classes
% [2] https://en.wikipedia.org/wiki/Confusion_matrix
% [3] http://www.assimp.org/main_viewer.html
% [4] https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model

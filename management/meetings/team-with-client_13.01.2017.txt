Rendez vous du 13 Janvier 2017 à 8H30 avec le client (Mr Paulin) avec l'équipe (Hugo, Yi, Rémi) : Compte Rendu

Nous avons étudié point par point, avec Mr Paulin, le plan de management initial que nous avions preparé afin de corriger d'eventuels points incorrects ou pas assez précis vis à vis des exigences du client.


Parties Prenantes : 
	- "Livrables" : Pas assez precis -> Exe + Doc.


Contenu :
	- Preciser version de OpenGL (pour améliorer la forme du document).
	- Ne pas promettre 60 FPS car ça ne sera sans doute pas réalisable. Plutot mettre 60 FPS en tant qu'objectif et non une certitude.
	- Blinn-Phong : Plus trop utilisé, voir pour "Micro Facettes" (Physic Based Rendering). Voir les articles pour Frosbyte (EA, Dice) sur le sujet pourrait être intéréssant (Cook-Torrance notamment).
	- Reglages proposés incorrects. Reglages suggérés : distance Focale, ouverture de lentille.
	- "compilateur standard de l'époque" pas correct comme formulation. Plutot mettre quelque chose comme : "compilateur supportant le c++11/14".
	- Le client nous a suggéré le C++11 pour ne pas "se compliquer la vie".
	- Traiter les warnings en tant qu'erreurs (-werror) pour aider au portage entre CLANG et GCC, car il n'y pas les même warnings des deux côtés et les warnings c'est pas bien.
	- Pour le paragraphe "si reste du temps", pas mettre scene plus complexe, car ça pourrait faire apparaître des problèmes non envisagé et donc des difficultées techniques en plus à la fin du developement, ce qui n'est pas souhaité. Le client nous a plutot suggéré d'implementer d'autres types d'effet de lentille s'il nous reste du temps, car c'est plus interessant et moins complexe que de faire de la creation de contenu correct pour une scene de test.
	- Schema WBS incomplet. Au lieu de Couches et Floutages dans le moteur, plutot montrer la decomposition en couches, le traitement qui sera effecté sur les couches, puis les reconstruction de l'image. Pour l'UI, il manque des réglages, il faudrait pouvoir ajuster la taille du "Film"(?), les caractéristiques de l'appareil simulé, la prise de vue et la précision de vue (la précision fera sans doute varier le temps de calcul). Aussi l'ajout de la possibilité de voir les differents layers peut être interessant.


Communication :
	- Le client est d'accord pour un rendez vous toutes les 2 semaines pour faire le point sur l'avancé du projet.


Risques :
	- Decomposer en WorkPackage puis -> voir dependances entre les tâches et les WorkPackages
					 -> risque : une des tâches échoue, quelles incidences par rapport aux dépendances ?
					 -> comment gerer ces échecs ?
	- Analyse du document, à préciser. Si analyse échoue, déclencher roue de secours (demander de l'aide à Mr Paulin).
	- Determiner si les risques sont fort/moyen/faible.
	- Determiner quand est-ce que ça peut arriver.
	- Ecart avec le sujet, non pertinent.
	- Risque possible : pas réussir la reconstruction. (trouvé en faisant la décomposition en WorkPackage).
	- Montrer l'interference avec les autres projets n'est pas pertinent dans les risques, il vaut mieux montrer ça dans le diagramme de Gantt qui est beaucoup plus fexible et mieux adapter pour ceci.
	- Risque possible : Non respect des delais.


Delais : 
	- Gantt presenté pas correct. Manque la phase d'analyse du problème (comprehension du document et mise en place des spécifications). Analyse -> Spécifications -> Conception -> Codage.
	(?) - Après analyse globale, il faut pouvoir faire un cahier de tests (tests unitaire).
	- Faire apparaître les livrables ou le degrè d'accomplissement d'une étape.


Qualité :
	- '_' pas 'm_' pour les attributs membres.
	- Fonctions aussi acceptées en truc_bidule_machin
	- Il ne faut pas que nos choix de nomenclatures ralentissent le codage.
	- Le client préfère les accolades comme suit : 
		if (cond) {
		}else{
		}
	- Pas besoin d'une limite de caractères par ligne.
	- La documentation sera bien faite via Doxygen. Les commentaires pour les prototypes des fonctions seront sous la forme JavaDoc, c'est à dire : 
		/**
		 * @param
		 */
	- Les blocks de commentaires des fonctions ne doivent pas récrire le titre de la fonction mais plutot préciser les assert qu'elle contient et les contraintes ou hypotheses d'utilisation.
	- Nous devrons donner au client un liens d'accès en lecture au repertoire BitBucket.
	- Regarder pour éssayer d'implementer l'integration continue (builds centralisés fait à chaque commit + tests unitaires pour s'assurer que le commit ne casse rien). Si l'implementation prend trop de temps (plus d'un jour), ne pas faire. (Voir Google Tests pour tests unitaires). Possibilité de parler avec les DL pour voir s'ils savent faire.
	- Le fichier latex disponible sur git est suffisant pour Mr Paulin, mais il nous reste à savoir si cela est acceptable pour les responsables de l'UE projet (Google Drive dans énoncé / Mr Migeon a evoqué un dépôt moodle ouvert Lundi 16 Janvier).
	- Si nous travaillons sur une scène de test en particulier, il faut qu'elle soit accessible sur git à la fois pour les tests unitaires mais aussi pour pouvoir être consulée par les parties prenantes.



Nous avons pu fixer un prochain rendez vous le Vendredi 27 Janvier 2017 de 8H à 10H


TD UE Projet du 26 Janvier 2017 à 10h avec Florent Mouysset et l'équipe (Hugo, Yi, Rémi) : Retours sur le plan de management initial

Mr Mouysset nous a montrer des soucis (mais aussi des bonnes choses) dans le plan de management initial que nous avons rendu le Lundi 16 Janvier 2016 par mail.


Global:
	- Il faut ajouter le nom du client en haut du document.
	- Il faut tracer les modifications apportées au plan dans un fichier côté accessible par le client et les responsables de l'UE projet.
	- Il faut ajouter une table des matières.
	- Il faut ajouter l'historique des versions du document avec les personnes impliquées dans les versions.
	- Il faut entrenir un fichier avec toutes les modifications apportées par chacun (pas forcement uniquement en rapport à ce document) dans un fichier, car les transmissions orals ça s'oublie et peuvent jouer des tours. Ce fichier est à pointer dans le plan de management.
	- Les conventions de code peuvent être placées dans un document à côté. On peut aussi le pointer dans le plan de management.


Parties prenantes:
	- Détailler le dernier paragraphe. "Certaines contraintes" pas assez précis.
	- "Livrables Papier" dans matrice RACI pas assez précis. Bien détailler ce que ça represente, pourquoi pas utiliser des arborescences.
	- Les parties prenantes peuvent (ont ?) un role : service qualité/ service juridique.
	- Il pourrait être plus clair d'avoir deux matrice RACI. Une pour le client, qui serait un peu plus globale dans les tâches et les rôle impliqués. Puis une deuxième pour décrire précisement entre les membres de l'équipe, le rôle de chacun.
	- Ajouter les noms dans la matrice pour la rendre plus claire.
	- Les rôles peuvent être tournant, si c'est le cas, il faut le préciser.
	- Relations client à expliquer / détailler.


Contenu:
	- Mieux construire cette partie, ça va un peu dans tous les sens.
	- Peut être organiser en "Exigences", "Objectifs", "Technologie" (pas dans l'ordre).
	- "Environement logiciel" pas "context".
	- Expliquer le schema Work Breakdown Structure, car il est la sans explications, on comprends pas trop.
	- "S'il nous reste du temps" pas terrible, plutot mettre "Si on a du temps supplémentaire".
	- Les exigences doivent être séparées en deux parties : les exigences fonctionnelles et les exigences non fonctionnelles. Les deux parties doivent être présentées distinctement dans le document.
	- Ne pas confondre "Activité" et "Tâche", une activité est une procédure récurrente. Une tâche est quelque chose de très spécifique et plus petit qu'une activité. Donc attention à "Activité"


Communication:
	- Il faut detailler.
	- On peut reciter git il n'y a pas de soucis car c'est un moyen de communication aussi.


Risques:
	- Peuvent être représentés en tableau ou matrice pour être plus clair.
	- Les tâches non réalisées c'est rare, elles sont plus souvent "mal" réalisées ou alors il peut y avoir des fonctionnalitées en plus non prévues.
	- Il faut quantifier les risques et leurs impacts.
	- Si possible arrivait à predire à quel moment de la production ça va se produire et comment faire pour éssayer des les empecher.
	- "Facteur Bus" pas connu.


Delais:
	- Il faut plutot parler en "itération".
	- On peut se tromper en estimant les itérations c'est pas grave, il faut juste bien détaillé et prevenir quand on rectifie sur le document.
	- Losange pour fin d'itération pas clair.
	- A rattacher avec OpenUp.
	- Eviter les itérations hebdomadaires, c'est trop court.


Qualité:
	- Expliquer pourquoi C++, git, etc sont des gages de qualité.
	- Associer des pratiques (?).
	- Expliquer pourquoi OpenUp augmente la qualité.
	- Les incréments sont des petites tâches accomplies (une fonctionnalité par exemple). Une itération est un ensemble d'incrément.
	- Les itérations doivent faire plus d'une semaine, sinon c'est trop court.
	- Rattacher Gantt à OpenUp.


Une nouvelle version améliorée du plan est à rendre avant le Mercredi 1  Fevrier 2017 par mail (ou drive) à Florent Mouysset.
Même si y a pas toutes les corrections c'est pas grave, il faut rendre quand même pour qu'il puisse voir où on en est.


TD avec Mr Mouysset, 23 Mars 2017 à 10h: Retours sur le plan de management

- Expliquer ce que c'est l'effet de Bokeh pour pas être "pompeux".
- "Le temps le permet" pas bien, plutot dire que c'est optionnel.
- Expliquer comment on forme / genère les itérations.
- Diagramme velocité bien.
- Partie méthodologie pas bien construite (OpenUP, Phases, ...)
- "Test des exigences" plutot mettre "Test de recette"
- Mettre en evidence les 2 effets de depth of field (f1.1, f1.2).
- Montrer leurs validation dans le déroulement de la demo.
- Au lieu de "cas critique" mettre "cas limite" ou "pire cas".

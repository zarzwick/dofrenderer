TD avec Mr Mouysset, 2 Fevrier 2017 à 10h: Retours sur le plan de management

- Mettre les responsables d'UE projet en tant que responsable juridique et de qualité.
- Ajouter les noms des gens dans la matrice RACI.

Outils :
	- Appuyer des propos ?
	- Skype -> pourquoi ça améliore la qualité.
	- Donner les versions des logiciels.
	- Mettre un lien vers mode d'emploi si outil pas très connu.


- Il faut mettre des liens pour acceder à chaque document externe qu'on cite.
- Reparer hyperliens (qt, assimp).
- Mettre les dates à côté des versions.
- La partie "intégration" pourrait être du même niveau que les parties prenantes et tout (une partie à part entière).
- Il peut y avoir plusieurs responsable qualité mais il faut préciser les noms.

Exigences Fonctionnelles :
	- Decouper plus, on peut mettre des n° pour relier au commit / bugtracker.
	- n° sur itération pour les même raisons.
	- n° aussi sur les items du work breakdown.
	- Determiner la charge en heure ou jour des items du work breakdown.
	- On peut tout numeroter même les exigences non fonctionnelles.
	
Qualité : 
	- Pas obligé de mettre version pour les plateformes (Linux/Mac) sauf si c'est une restriction.
	- On peut preciser sur quelles machines ont été testé le projet.
	- On mélange activités et fonctionnalités.
	- Decomposer les éxigences en activités.
	- Increment/Iteration -> argumenter (mieux detailler).

Objectifs : 
	- Trop de redite.
	- On voit pas trop pourquoi c'est là.
	- Il faut détaillé en terme d'iterations.

Communication :
	- Il faut mieux structurer	-> Com intern
					-> Com extern
	

Risques : 
	- "plus grand chose à montrer" à virer.
	- Tableau risque	-> il faut qualifier (pas de numeros mais des termes comme "elevé", ou en frequence ?).
				-> "risque" à changer pour "Probabilité que ça arrive"
				-> "danger" à changer pour "Impact"

Gantt :
	- Decompoer les trucs en parrallele pour former des itérations (?).
	- Dire si il y a du retard ou si on a reussi.
	- On voit pas trop les itérations sur le gantt.

- Faire des rapports d'analyse, fichier de spec ou un fichier presentant l'architecture du projet (peut etre un schema). Parler ce des documents et mettre un liens d'acces dans "Contenu".
- L'architecture peut aussi être mentionné dans "Qualité" pour y être justifiée.



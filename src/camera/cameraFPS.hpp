#ifndef M1_DOFRDR_CAMERA_FPS_H
#define M1_DOFRDR_CAMERA_FPS_H

#include "camera.hpp"

/**
 * Camera with classic FPS controls (mouse + ZQSD/WASD).
 */
class CameraFPS : public Camera
{
public:
    /** Possible direction the FPS camera. */
    enum Direction
    {
	Forward = 1,
	Backward = 2,
	Left = 4,
	Right = 8
    };
    
public:
    /**
    * Default constructor.
    *  
    * @param position Starting 3D position of the camera.
    * @param yaw Starting yaw rotation (arround Y axis).
    * @param pitch Initial pitch rotation (arround X axis).
    */
    CameraFPS( const glm::vec3& position = glm::vec3 { 0.0f, 0.0f, 0.0f }, GLfloat yaw = 90.0f, GLfloat pitch = 0.0f );
    
    /**
     * Move according to a direction. Forward and backward follow camera foward.
     * Left and rien follow camera right.
     * 
     * @param direction Direction to move the camera to.
     * @param deltaTime Used to make movement flawless and framerate independant.
     */
    void processKeyboard( Direction direction, GLfloat deltaTime );
    
    /**
     * Rotate the camera according to a mouse movement.
     *
     * @param offset Mouse delta movement.
     * @param constrainPitch If true, make sure the screen doesn't get flipped.
     */
    void processMouseMove( glm::vec2 offset, GLboolean constrainPitch = true );
    
    /**
     * Change the field of view according to the mouse scroll.
     * 
     * @param delta Delta of the mouse scroll.
     */
    void processMouseScroll( GLfloat delta );
    
};


#endif

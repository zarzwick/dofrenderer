#include "film.hpp"

using glm::vec2;

// Film presets.
vec2 Film::_size[Film::FILM_COUNT] = { vec2(36.f, 24.f), vec2(23.6f, 15.7f) };


Film::Film( Frame type )
    : _frametype( type )
{
    // ...
}


vec2& Film::size() const
{
    return _size[_frametype];
}


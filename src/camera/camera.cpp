#include "camera.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <QtCore/QDebug>

Camera::Camera( const glm::vec3& position, GLfloat yaw, GLfloat pitch )
    : _forward{ 0.0f, 0.0f, 1.0f }
    , _fov{ MAX_FOV }
    , _moveSpeed{ 3.0f }
    , _rotationSpeed{ 0.25f }
{
    _position = position;
    _yaw = yaw;
    _pitch = pitch;
    updateVectors();
}

glm::mat4 Camera::getViewMatrix() const
{
    return glm::lookAt( _position, _position + _forward, _up );
}

void Camera::updateVectors()
{
    // Process forward with camera rotations.
    _forward.x = cos( glm::radians( _yaw ) ) * cos( glm::radians( _pitch ) );
    _forward.y = sin( glm::radians( _pitch ) );
    _forward.z = sin( glm::radians( _yaw ) ) * cos( glm::radians( _pitch ) );
    _forward = glm::normalize( _forward );

    // Process right and up according to the new forward vector.
    _right = glm::normalize( glm::cross( _forward, glm::vec3{ 0.0f, 1.0f, 0.0f } ) );
    _up = glm::normalize( glm::cross( _right, _forward ) );
}

void Camera::SetPosition( const glm::vec3& position )
{
    _position = position;
}

glm::vec3 Camera::getPosition() const
{
    return _position;
}

void Camera::move( const glm::vec3& offset)
{
    _position += offset;
}
    
void Camera::setRotation( const glm::vec3& eulerAngles, GLboolean constrainPitch )
{
    _pitch = eulerAngles.x;
    _yaw = eulerAngles.y;
    _roll = eulerAngles.z;
    
    if( constrainPitch )
	glm::clamp( MIN_PITCH, MAX_PITCH, _pitch );
    
    updateVectors();
}
    
void Camera::rotate( const glm::vec3& eulerAngles, GLboolean constrainPitch )
{
    _pitch += eulerAngles.x;
    _yaw += eulerAngles.y;
    _roll += eulerAngles.z;
    
    if( constrainPitch )
	glm::clamp( MIN_PITCH, MAX_PITCH, _pitch );
    
    updateVectors();
}

void Camera::setMovementSpeed( GLfloat speed )
{
    _moveSpeed = speed;
}
GLfloat Camera::getMovementSpeed() const
{
    return _moveSpeed;
}
void Camera::setRotationSpeed( GLfloat speed )
{
    _rotationSpeed = speed;
}
GLfloat Camera::getRotationSpeed() const
{
    return _rotationSpeed;
}

void Camera::setFOV( GLfloat fov )
{
    _fov = fov;
    
    glm::clamp( MIN_FOV, MAX_FOV, _fov );
}
GLfloat Camera::getFOV() const
{
    return _fov;
}

const glm::vec3& Camera::getForward() const
{
    return _forward;
}
const glm::vec3& Camera::getRight() const
{
    return _right;
}
const glm::vec3 Camera::getUp() const
{
    return _up;
}

#ifndef M1_DOFRDR_CAMERA_H
#define M1_DOFRDR_CAMERA_H

#include "../opengl.hpp"
#include <glm/glm.hpp>

/**
 * 3D Camera.
 * Represent a position and an orientation used for rendering projections.
 * The orientation is defined by the forward, right and up vectors. (Right handed)
 */
class Camera
{
public:
    /** 
    * Default constructor. 
    * 
    * @param position Starting 3D position of the camera.
    * @param yaw Starting yaw rotation (arround Y axis).
    * @param pitch Initial pitch rotation (arround X axis).
    */
    Camera( const glm::vec3& position = glm::vec3{ 0.0f, 0.0f, 0.0f }, GLfloat yaw = 90.0f, GLfloat pitch = 0.0f );
  
    /** Default destructor. */
   virtual ~Camera() = default;
    
    /**
    * Get a view matrix corresponding to the camera looking at a point (position + forward).
    * 
    * @return Matrix 4x4 representing the camera view.
    */
    glm::mat4 getViewMatrix() const;
  
    
    
    /**
     * @param position New 3D position to apply to the camera.
     */
    void SetPosition( const glm::vec3& position );
    
    /**
     * Get the camera position
     */
    glm::vec3 getPosition() const;
    
    /**
     * Translate the camera, add the offset to the current location.
     * 
     * @param offset Translation to apply
     */
    void move( const glm::vec3& offset);
    
    /**
     * @param eulerAngles Angles to apply to the camera (x:pitch, y:yaw, z:roll).
     * @param constrainPitch If true, make sure the screen doesn't get flipped.
     */
    void setRotation( const glm::vec3& eulerAngles, GLboolean constrainPitch = true );
    /**
     * Add a rotation to the current one.
     * 
     * @param eulerAngles Angles to cumulate with current (x:pitch, y:yaw, z:roll).
     * @param constrainPitch If true, make sure the screen doesn't get flipped.
     */
    void rotate( const glm::vec3& eulerAngles, GLboolean constrainPitch = true );
    
    /**
     *  @param speed New speed to apply to the camera movements.
     */
    void setMovementSpeed( GLfloat speed );
    /** 
     * @return Movement speed.
     */
    GLfloat getMovementSpeed() const;
    /**
     * @param speed New rotation speed to apply to the camera.
     */
    void setRotationSpeed( GLfloat speed );
    /**
     * @return Rotation speed.
     */
    GLfloat getRotationSpeed() const;
  
    
    /**
     * @param fov New field of view value in degrees.
     */
    void setFOV( GLfloat fov );
    /**
     * @return Field of view in degrees.
     */
    GLfloat getFOV() const;
    
    
    /**
     * @return Forward of the camera.
     */
    const glm::vec3& getForward() const;
    
    /**
     * @return Right of the camera.
     */
    const glm::vec3& getRight() const;
    
    /**
     * @return Up of the camera.
     */
    const glm::vec3 getUp() const;
    
private:
    /**
    * Process new forward vector with camera rotations.
    * Then update right and up vector with the new forward.
    */
    void updateVectors();
  
private:
    /** 3D position of the camera in world space. */
    glm::vec3 _position;

    /** Forward vector of the camera. Represent in which direction the camera is oriented. */
    glm::vec3 _forward;

    /** Right camera vector. Help to orient the camera. */
    glm::vec3 _right;

    /** Up camera vector. Help to orien the camera. */
    glm::vec3 _up;

    /** Rotation around Y axis. */
    GLfloat _yaw;

    /** Rotation around Z axis. */
    GLfloat _roll;

    /** Rotation around X axis. */
    GLfloat _pitch;

    /** Field of view ( zoom ). */
    GLfloat _fov;

    /** Movement speed (positions modifications). */
    GLfloat _moveSpeed;

    /** Rotation sensibility (yaw, pitch, roll modifications). */
    GLfloat _rotationSpeed;

    
    // Defines
private:
    static constexpr GLfloat MAX_FOV = 45.0f;
    static constexpr GLfloat MIN_FOV = 1.0f;
    static constexpr GLfloat MAX_PITCH = 89.0f;
    static constexpr GLfloat MIN_PITCH = -89.0f;
};


#endif




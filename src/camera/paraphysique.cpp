#include "paraphysique.hpp"

#include "../decomposition/decomposition.hpp"

OpticsSettings::OpticsSettings()
  : applyNearToWorld { true }
  , applyFarToWorld { true }
  , _focusDistance { Decomposition::meterToMillimeter( 2.0 ) }
  , _nearDistance { Decomposition::meterToMillimeter( 1.0 ) }
  , _farDistance { Decomposition::meterToMillimeter( 100.0 ) }
  , _focalLength { 55.0 }
  , _aperture { 40.0 }
{
    updateLensRadius();
}

void OpticsSettings::setFocalLength( const double&focalLength, bool convertToMilimeter )
{
    _focalLength = convertToMilimeter ? Decomposition::meterToMillimeter(focalLength) : focalLength;

    updateLensRadius();
}

void OpticsSettings::setFocusDistance( const double& focusDistance, bool convertToMilimeter )
{
    _focusDistance = convertToMilimeter ? Decomposition::meterToMillimeter(focusDistance) : focusDistance;

    checkOpticsRanges();
}

void OpticsSettings::setNearDistance( const double& nearDistance, bool convertToMilimeter )
{
    _nearDistance = convertToMilimeter ? Decomposition::meterToMillimeter(nearDistance) : nearDistance;

    checkOpticsRanges();
}

void OpticsSettings::setFarDistance( const double& farDistance, bool convertToMilimeter )
{
    _farDistance = convertToMilimeter ? Decomposition::meterToMillimeter(farDistance) : farDistance;

    checkOpticsRanges();
}

void OpticsSettings::setAperture( const double& aperture )
{
    _aperture = aperture;

    updateLensRadius();
}

void OpticsSettings::setFilmType( const Film::Frame type )
{
    _film._frametype = type;
}

double OpticsSettings::getFocusDistance(bool convertToMeter) const
{
    return ( convertToMeter ? Decomposition::millimeterToMeter(_focusDistance) : _focusDistance );
}

double OpticsSettings::getFocalLength( bool convertToMeter ) const
{
    return ( convertToMeter ? Decomposition::millimeterToMeter(_focalLength) : _focalLength );
}

double OpticsSettings::getLensRadius( bool convertToMeter ) const
{
    return ( convertToMeter ? Decomposition::millimeterToMeter(_lensRadius) : _lensRadius );
}

double OpticsSettings::getNearDistance( bool convertToMeter ) const
{
    return ( convertToMeter ? Decomposition::millimeterToMeter(_nearDistance) : _nearDistance );
}

double OpticsSettings::getFarDistance( bool convertToMeter ) const
{
    return ( convertToMeter ? Decomposition::millimeterToMeter(_farDistance) : _farDistance );
}

double OpticsSettings::getAperture() const
{
    return _aperture;
}

glm::vec2& OpticsSettings::getFilmSize() const
{
    return _film.size();
}

Film::Frame OpticsSettings::getFilmType() const
{
    return _film._frametype;
}

void OpticsSettings::updateLensRadius()
{
    // Diameter = _focalLength / _aperture
    // Radius = ( _focalLength / _aperture ) / 2

    _lensRadius = _focalLength / ( _aperture * 2.0 );
}

void OpticsSettings::checkOpticsRanges()
{
    const double& min = Decomposition::meterToMillimeter( 0.01 );

    _nearDistance = qBound( min, _nearDistance, _farDistance - min );

    if( _farDistance <= _nearDistance )
        _farDistance = _nearDistance + min;

    //_nearDistance = qBound( std::numeric_limits<double>::epsilon(), _nearDistance, _farDistance - std::numeric_limits<double>::epsilon() );

    _focusDistance = qBound( _nearDistance, _focusDistance, _farDistance );
}

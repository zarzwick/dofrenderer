#ifndef paraphysique_hpp
#define paraphysique_hpp

#include "film.hpp"

#include <stdio.h>

#include <QMainWindow>
#include <QOpenGLWidget>

/**
 * Class that store optics settings.
 * Store lens settings like focal length or aperture.
 * Store also depth of field settings like minimum and maximum
 * distance that we can focus on and the focus distance.
 */
class OpticsSettings
{
public:    
    /** Default constructor. */
    OpticsSettings();

    /**
     * @param focalLength New focal length value. In millimeter (or set true for second parameter).
     * @param convertToMilimeter If true, convert the value from meter to millimeter.
     */
    void setFocalLength( const double& focalLength, bool convertToMilimeter = false );

    /**
     * @param focusDistance New value for the focus distance. In millimeter (or set true for second parameter).
     * @param convertToMilimeter If true, convert the value from meter to millimeter.
     */
    void setFocusDistance( const double& focusDistance, bool convertToMilimeter = false );

    /**
     * @param nearDistance New value for the minimum distance that we can focus on. In millimeter (or set true for second parameter).
     * @param convertToMilimeter If true, convert the value from meter to millimeter.
     */
    void setNearDistance( const double& nearDistance, bool convertToMilimeter = false );

    /**
     * @param farDistance New value for the maximum distance that we can focus on. In millimeter (or set true for second parameter).
     * @param convertToMilimeter If true, convert the value from meter to millimeter.
     */
    void setFarDistance( const double& farDistance, bool convertToMilimeter = false );

    /**
     * @param aperture New value for lens aperture.
     */
    void setAperture( const double& aperture );

    /**
     * Change frame type.
     * @param type The Frame preset to use.
     */
    void setFilmType( const Film::Frame type );

    /**
     * @param convertToMeter If true, convert the value from millimeter to meter.
     * @return Current optics focus distance. In millimeters (or set true for second parameter if you want meters).
     */
    double getFocusDistance( bool convertToMeter = false ) const;

    /**
     * @param convertToMeter If true, convert the value from millimeter to meter.
     * @return Current optics focal length. In millimeters (or set true for second parameter if you want meters).
     */
    double getFocalLength( bool convertToMeter = false ) const;

    /**
     * @param convertToMeter If true, convert the value from millimeter to meter.
     * @return Current optics lens radius. In millimeters (or set true for second parameter if you want meters).
     */
    double getLensRadius( bool convertToMeter = false ) const;

    /**
     * @param convertToMeter If true, convert the value from millimeter to meter.
     * @return Current minimum distance that we can focus on. In millimeters (or set true for second parameter if you want meters).
     */
    double getNearDistance( bool convertToMeter = false ) const;

    /**
     * @param convertToMeter If true, convert the value from millimeter to meter.
     * @return Current maximum distance that we can focus on. In millimeters (or set true for second parameter if you want meters).
     */
    double getFarDistance( bool convertToMeter = false ) const;

    /**
     * @return Current lens aperture.
     */
    double getAperture() const;

    /**
     * @return The film size.
     */
    glm::vec2& getFilmSize() const;

    /**
     * @return The film preset.
     */
    Film::Frame getFilmType() const;

private:
    /** Process the lens radius from focal length and aperture. */
    void updateLensRadius();

    /** Check boundaries of near, far and focus distance. */
    void checkOpticsRanges();

public:
    /** If true, the near distance will be used as the near plane distance for rendering. */
    bool applyNearToWorld;

    /** If true, the far distance will be used as the far plane distance for rendering. */
    bool applyFarToWorld;

private:
    /** "df" */
    double _focusDistance;

    /** "dnear". Minimum distance that we can focus on. */
    double _nearDistance;

    /** Maximum distance that we can focus on. */
    double _farDistance;

    /** "F" */
    double _focalLength;

    /** "E" */
    double _lensRadius;

    /** Used to process lens radius (E). */
    double _aperture;

    /** Frame caracteristics. */
    Film _film;
   
};

#endif /* paraphysique_hpp */


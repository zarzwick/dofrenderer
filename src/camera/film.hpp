#ifndef M1_DOFRDR_FILM_HPP
#define M1_DOFRDR_FILM_HPP

#include <glm/glm.hpp>


/**
 * Representation of the camera frame. Mostly wikipedia data.
 */
struct Film {

    enum Frame {
        FILM_FULL_FRAME = 0,
        FILM_APS_C,
        // ...
        FILM_COUNT
    };

    /**
     * Default constructor.
     * @param type Frame preset.
     */
    Film( Frame type = FILM_FULL_FRAME );

    /**
     * The frame size in mm.
     * @return A vec2 with x and y film size.
     */
    glm::vec2& size() const;

public:
    /** Frame preset. */
    Frame _frametype;

    /** Frame types size reference. Must match the Frame enum. */
    static glm::vec2 _size[FILM_COUNT];

}; // end Film.


#endif


#include "cameraFPS.hpp"

CameraFPS::CameraFPS ( const glm::vec3& position, GLfloat yaw, GLfloat pitch ) 
    : Camera{ position, yaw, pitch }
{

}

void CameraFPS::processKeyboard(Direction direction, GLfloat deltaTime )
{
    GLfloat velocity = getMovementSpeed() * deltaTime;
    glm::vec3 translation{ 0.0f, 0.0f, 0.0f };
    
    if( direction & Direction::Forward )
        translation += getForward() * velocity;
    
    if( direction & Direction::Backward )
        translation -= getForward() * velocity;
    
    if( direction & Direction::Right )
        translation += getRight() * velocity;
    
    if( direction & Direction::Left )
        translation -= getRight() * velocity;
    
    move( translation );
}
    
void CameraFPS::processMouseMove( glm::vec2 offset, GLboolean constrainPitch )
{
    offset *= getRotationSpeed();
    
    const glm::vec3 angles{ offset.y, -offset.x, 0.0f };
    
    rotate( angles, constrainPitch );
}
    
void CameraFPS::processMouseScroll( GLfloat delta )
{
    setFOV( getFOV() - delta );
}


#ifndef M1_DOFRDR_POISSONDISK_HPP
#define M1_DOFRDR_POISSONDISK_HPP

#include <glm/glm.hpp>
#include <utility>
#include <random>
#include <vector>


template <typename T> struct Grid2D;


/**
 * Poisson disk samples generator.
 * Provide N samples on a domain, with samples roughly evenly spaced.
 * 2D implementation of
 * Robert Bridson. 2007. Fast Poisson disk sampling in arbitrary dimensions.
 * In ACM SIGGRAPH 2007 sketches (SIGGRAPH '07). ACM, New York, NY, USA, Article 22.
 */
class PoissonDisk2D {

public:
    /**
     * Default constructor.
     */
    PoissonDisk2D();

    /**
     * Generate N Poisson disks samples over the given domain.
     * @param N The number of samples to generate.
     * @param r The minimum distance between two samples.
     * @param origin The central point of the lens. (0.f, 0.f) is a good exemple.
     * @param radius The radius of the lens.
     * @param k The upper bound when testing new points arround an existing sample.
     * @return The number of samples generated.
     */
    uint generate( uint N, float r, glm::vec2 origin, float radius, uint k = 30 );

    /**
     * Provide access to the samples.
     */
    glm::vec2& operator[]( unsigned int i );

    /**
     * Access the underlying container raw data.
     */
    std::vector<glm::vec2>& data();

private:
    /**
     * Return a random points in the spherical annulus [r, 2r] of p.
     * @param p The original point.
     * @param r The radius for the annulus.
     */
    glm::vec2 surroundingPoint( glm::vec2& p, float r );

    /**
     * Check if a point is valid, e.g if it isn't too near from another point.
     * @param p The point to check.
     * @param cell The cell containing p.
     * @param grid The grid storing the indices with their location.
     * @param sizeFactor Inverse of the size of a cell.
     * @param rows Number of rows in grid.
     * @param columns Number of columns in grid.
     * @param r Minimum distance between samples.
     */
    bool checkNeigbourhood( glm::vec2& p, glm::ivec2& cell, Grid2D<int>& grid, float sizeFactor, int rows, int cols, float r );

private:
    /** Vector of samples. */
    std::vector<glm::vec2> _samples;

    /** Uniform distribution engine. */
    std::uniform_real_distribution<float> _urng;
    std::mt19937 _mt;

}; // end PoissonDisk

#endif


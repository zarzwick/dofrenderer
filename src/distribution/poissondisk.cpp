#include "poissondisk.hpp"

#include <iostream>
#include <cmath>

using glm::vec2;
using glm::ivec2;
using namespace std;



/**
 * Local grid type (a big (w*h) vector with custom access).
 */
template<typename T>
struct Grid2D {

    Grid2D(uint h, uint w) : _w( w ), _h( h ) { _data.resize(w*h); };

    T& operator()(uint x, uint y) { return _data[y*_w + x]; };

    size_t size() const { return _data.size(); };

    vector<T> _data;
    uint _w;
    uint _h;

}; // end Grid2D



static float distanceSquared( vec2& a, vec2& b )
{
    return powf(b.x - a.x, 2) + powf(b.y - a.y, 2);
}



PoissonDisk2D::PoissonDisk2D()
    : _samples()
    , _urng( 0.f, 1.f )
    , _mt()
{
    random_device rd;
    _mt.seed(rd());
}



uint PoissonDisk2D::generate( uint N, float r, glm::vec2 origin, float radius, uint k )
{
    _samples.clear();


    // Make room for the N future samples.
    _samples.reserve( N );


    // 0. Init.
    vec2 offset = vec2( radius );
    float sizeFactor = sqrtf(2) / r; // WARNING: Inverse of the size of a cell, for efficiency.
    float radius2 = radius*radius;
    int w = 2*radius, h = 2*radius;
    int rows = floor(w * sizeFactor), columns = floor(h * sizeFactor);

    // The sequence of active samples.
    vector<int> actives;

    // As said in the paper, each grid cell contains the index of the sample it contains, or -1.
    Grid2D<int> grid( rows, columns );
    grid._data.assign( grid.size(), -1 );


    // 1. Create the first sample at center of the domain.
    vec2 current = origin;
    ivec2 cell = ivec2( floor((current.x+offset.x)*sizeFactor), floor((current.y+offset.y)*sizeFactor) );

    grid( cell.x, cell.y ) = 0;
    _samples.push_back( current );
    actives.push_back( 0 );


    // 2. Create the points with respect to existing ones.
    while ( (_samples.size() < N) && (! actives.empty ()) )
    {
        // Take a random point in the active pool.
        int idx = rand() % actives.size();
        int i = actives[idx];

        bool valid = false;
        uint iteration = 0;

        // Try k times to generate a point from samples[i].
        while ( (! valid) && (iteration < k) )
        {
            ++ iteration;

            current = surroundingPoint( _samples[i], r );
            cell = ivec2( floor((current.x+offset.x)*sizeFactor), floor((current.y+offset.y)*sizeFactor) );

            // Check it belongs to the domain.
            if ( distanceSquared(current, _samples[0]) > radius2 )
                continue;

            valid = checkNeigbourhood( current, cell, grid, sizeFactor, rows, columns, r );
        }

        // If a valid new point was found, register it.
        if ( valid )
        {
            actives.push_back( _samples.size() );
            grid( cell.x, cell.y ) = _samples.size();
            _samples.push_back( current );
        }
        // If no point was found, remove the i from active pool
        else
        {
            actives.erase( actives.begin() + idx );
        }
    }

    //_samples.shrink_to_fit();
    return _samples.size();
}



vec2& PoissonDisk2D::operator[]( uint i )
{
    return _samples[i];
}


std::vector<vec2>& PoissonDisk2D::data()
{
    return _samples;
}


vec2 PoissonDisk2D::surroundingPoint( vec2& p, float r )
{
    float dist = _urng(_mt)*r + r;
    float teta = _urng(_mt)*2*M_PI;

    return vec2( p.x + dist*cosf(teta), p.y + dist*sinf(teta) );
}



bool PoissonDisk2D::checkNeigbourhood( vec2& p, ivec2& cell, Grid2D<int>& grid, float sizeFactor, int rows, int cols, float r )
{
    int i;
    float r2 = r*r;

    // If the cell's already busy.
    if ( grid(cell.x, cell.y) >= 0 )
        return false;

    // Test against nearby cells (grid's 8-neighbours).
    for ( int y = -1; y < 2; ++ y )
    {
        for ( int x = -1; x < 2; ++ x )
        {
            if ( (x == 0 && y == 0) || cell.x + x < 0 || cell.x + x >= cols || cell.y + y < 0 || cell.y + y >= rows )
                break;

            i = grid( cell.x + x, cell.y + y );

            if ( ( i >= 0 ) && ( distanceSquared(p, _samples[i]) < r2 ) )
                return false;
        }
    }

    return true;
}


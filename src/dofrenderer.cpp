#include "dofrenderer.hpp"
#include "camera/paraphysique.hpp"
#include "ui_dofrenderer.h"
#include "popUps/about/popupabout.h"
#include "popUps/help/popuphelp.h"
#include "scene/scene.hpp"

#include <QDir>
#include <QString>
#include <QSlider>
#include <QFileDialog>

using namespace std;

#include <QtMath>
#include <functional>


DOFRenderer::DOFRenderer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DOFRenderer)
{
    ui->setupUi(this);

    ui->drawDepthCheckbox->setDisabled(1);
    ui->debugColorCheckbox->setDisabled(1);

    _popUpAbout = new PopUpAbout(this);
    _popUpHelp = new PopUpHelp(this);

    std::function<void()> callback = std::bind( &DOFRenderer::onInitializeOpenGL, this );
    ui->openGLWidget->setUIOnInitializeOpenGLCallback( callback );
}

DOFRenderer::~DOFRenderer()
{
    if( _popUpAbout != nullptr )
        delete _popUpAbout;

    if( _popUpHelp != nullptr )
        delete _popUpHelp;

    delete ui;
}

void DOFRenderer::on_objPropPosX_valueChanged(double x)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropPosY_valueChanged(double y)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropPosZ_valueChanged(double z)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropRotX_valueChanged(double x)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropRotY_valueChanged(double y)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropRotZ_valueChanged(double z)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropScalX_valueChanged(double x)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropScalY_valueChanged(double y)
{
	transformMatrixChanged();
}

void DOFRenderer::on_objPropScalZ_valueChanged(double z)
{
	transformMatrixChanged();
}

void DOFRenderer::onInitializeOpenGL( DOFRenderer* app )
{
    const uint maxLayerCount = app->ui->openGLWidget->getMaxLayerCount();
    const uint layerCount = app->ui->openGLWidget->getLayerCount();

    app->ui->layerCountSlider->setMinimum( 1 );
    app->ui->layerCountSlider->setMaximum( maxLayerCount );
    app->ui->layerCountSlider->setSliderPosition( layerCount );

    app->ui->layerCountLabel->setText( QString::asprintf("Layer Count : %d", layerCount ) );

    app->ui->layerToRenderSlider->setMinimum( 0 );
    app->ui->layerToRenderSlider->setMaximum( layerCount - 1 );
    app->ui->layerToRenderSlider->setSliderPosition( 0 );

    app->ui->layerToRenderLabel->setText( QString::asprintf("Layer To Render : %d", 0 ) );


    // Initialize UI for optics settings.
    app->updateApertureUI();
    app->updateFocalLengthUI();
    app->updateFocusDistanceUI();
    app->updateFarUI();
    app->updateNearUI();
}

void DOFRenderer::on_playButton_clicked()
{
    ui->openGLWidget->playButtonEvent();
}

void DOFRenderer::on_pauseButton_clicked()
{
    ui->openGLWidget->pauseButtonEvent();
}

void DOFRenderer::on_stopButton_clicked()
{
    ui->openGLWidget->stopButtonEvent();
}

void DOFRenderer::on_addModelButton_clicked()
{
    QString modelFile =
        QFileDialog::getOpenFileName( this, "Open model", QDir::currentPath() + "../../assets/", "Objects files (*obj)" );

    // Abort on user cancel.
    if ( modelFile.isNull() )
        return;

    // Add the model in the model list
    ui->openGLWidget->addModel( modelFile.toStdString().c_str() );

    // And rebuild outliner's list (the simplest way to manage adding a set of models)
    rebuildModelList();
}

void DOFRenderer::on_removeModelButton_clicked()
{
    // Get the selected widget items
    QList<QListWidgetItem*> items = ui->modelList->selectedItems();
    uint offset = 0;

    // And deleeeeeete
    for ( QListWidgetItem* item : items )
    {
        delete ui->modelList->takeItem( ui->modelList->row(item) );
        ui->openGLWidget->removeModel( ((ModelItem*) item)->_modelID - (offset++) );
    }

    // Rebuild list
    rebuildModelList();
}

void DOFRenderer::on_reloadSceneButton_clicked()
{
    ui->modelList->clear();
    ui->openGLWidget->removeAll();
    ui->objPropPosX->setValue(0.0);
    ui->objPropPosY->setValue(0.0);
    ui->objPropPosZ->setValue(0.0);
    ui->objPropRotX->setValue(1.0);
    ui->objPropRotY->setValue(1.0);
    ui->objPropRotZ->setValue(1.0);
    ui->objPropScalX->setValue(1.0);
    ui->objPropScalY->setValue(1.0);
    ui->objPropScalZ->setValue(1.0);

}

void DOFRenderer::on_sliderAperture_valueChanged(int value)
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    // Slider values are integers from 0 to 99.
    // Must bring back to value link to focal length.
    const double& focalLength = settings.getFocalLength();
    const double& percent = static_cast<double>(value) / 100.0;

    settings.setAperture( focalLength * percent );

    updateApertureUI();
}


void DOFRenderer::on_spinBoxFocalLength_editingFinished()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    settings.setFocalLength( ui->spinBoxFocalLength->value() );

    updateFocalLengthUI();
    updateApertureUI();
}

void DOFRenderer::on_spinBoxFocusDistance_editingFinished()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    settings.setFocusDistance(ui->spinBoxFocusDistance->value(), true);

    updateFocusDistanceUI();
}

void DOFRenderer::on_spinBoxFar_editingFinished()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    settings.setFarDistance(ui->spinBoxFar->value(), true);

    updateNearUI();
    updateFarUI();
    updateFocusDistanceUI();
}

void DOFRenderer::on_spinBoxNear_editingFinished()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    settings.setNearDistance(ui->spinBoxNear->value(), true);

    updateNearUI();
    updateFarUI();
    updateFocusDistanceUI();
}

void DOFRenderer::updateFocusDistanceUI()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    const double& focusDistance = settings.getFocusDistance(true);

    ui->spinBoxFocusDistance->setValue( focusDistance );
    ui->labelValueFocusDistance->setText( QString::number(focusDistance) );
}

void DOFRenderer::updateFocalLengthUI()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    const double& focalLength = settings.getFocalLength();
    ui->spinBoxFocalLength->setValue( focalLength );
    ui->labelValueFocalLength->setText( QString::number(focalLength) );
}

void DOFRenderer::updateApertureUI()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    const int apeture = qRound( ( settings.getAperture() / settings.getFocalLength() ) * 100.0);
    ui->sliderAperture->setValue( apeture );
    ui->labelValueAperture->setText( QString::number(settings.getAperture()) );
}

void DOFRenderer::updateFarUI()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    const double& far = settings.getFarDistance(true);
    ui->spinBoxFar->setValue( far );
    ui->labelValueFar->setText( QString::number(far) );
}

void DOFRenderer::updateNearUI()
{
    OpticsSettings& settings = ui->openGLWidget->settings;

    const double& near = settings.getNearDistance(true);

    ui->spinBoxNear->setValue( near );
    ui->labelValueNear->setText( QString::number(near) );
}

void DOFRenderer::on_checkBoxFar_toggled(bool checked)
{
    ui->openGLWidget->settings.applyFarToWorld = checked;
}

void DOFRenderer::on_checkBoxNear_toggled(bool checked)
{
    ui->openGLWidget->settings.applyNearToWorld = checked;
}

void DOFRenderer::on_framePresetComboBox_currentIndexChanged(int index)
{
    ui->openGLWidget->setFilmType( static_cast<Film::Frame>(index) );
}

void DOFRenderer::transformMatrixChanged()
{
    // Get the selected widget items
    QList<QListWidgetItem*> items = ui->modelList->selectedItems();

    // And modify all these
    for ( QListWidgetItem* item : items )
    {
        ui->openGLWidget->transformModel(
            ((ModelItem*) item)->_modelID,
            glm::vec3(ui->objPropPosX->value(), ui->objPropPosY->value(), ui->objPropPosZ->value()),
            glm::vec3(ui->objPropRotX->value(), ui->objPropRotY->value(), ui->objPropRotZ->value()),
            glm::vec3(ui->objPropScalX->value(), ui->objPropScalY->value(), ui->objPropScalZ->value())
        );
    }
}

void DOFRenderer::rebuildModelList()
{
    std::list<std::string> modelNames = ui->openGLWidget->getModelsNames();
    uint i = 0;

    ui->modelList->clear();

    for ( std::string const& name: modelNames )
        ui->modelList->addItem( new ModelItem{ name.c_str(), i++, ui->modelList } );
}


void DOFRenderer::on_actionAbout_triggered()
{
    if( _popUpAbout != nullptr )
        _popUpAbout->show();
}

void DOFRenderer::on_actionHelp_triggered()
{
    if( _popUpHelp != nullptr )
        _popUpHelp->show();
}

void DOFRenderer::on_radioButtonOneLayer_toggled( bool checked )
{
    if ( checked )
    {
        ui->openGLWidget->toggleDisplayMode();

        // Enable debug mode buttons.
        ui->drawDepthCheckbox->setEnabled(1);
        ui->debugColorCheckbox->setEnabled(1);
    }
}

void DOFRenderer::on_radioButtonRecompose_toggled( bool checked )
{
    if ( checked )
    {
        ui->openGLWidget->toggleDisplayMode();

        // Disable debug mode buttons.
        ui->drawDepthCheckbox->setDisabled(1);
        ui->debugColorCheckbox->setDisabled(1);
    }
}

void DOFRenderer::on_toggleWireframe_toggled(bool toggle)
{
     ui->openGLWidget->toggleWireframe(toggle);
}

void DOFRenderer::on_layerCountSlider_valueChanged(int value)
{
    if( !ui->openGLWidget->isOpenGLInitialized() )
        return;

    // We get back the value in case that it was clamped.
    const uint layerCount = ui->openGLWidget->setLayerCount( value );

    // If clamped, apply it to the slider.
    if( layerCount != static_cast<uint>(value) )
        ui->layerCountSlider->setSliderPosition( layerCount );

    ui->layerToRenderSlider->setMaximum( layerCount - 1 );

    // Update the label with the new info.
    ui->layerCountLabel->setText( QString::asprintf("Layer Count : %d", layerCount ) );
}

void DOFRenderer::on_layerToRenderSlider_valueChanged(int value)
{
    if( !ui->openGLWidget->isOpenGLInitialized() )
        return;

    // We get back the value in case that it was clamped.
    const uint layerToRender = ui->openGLWidget->setLayerToRender( value );

    // If clamped, apply it to the slider.
    if( layerToRender != static_cast<uint>(value) )
        ui->layerToRenderSlider->setSliderPosition( layerToRender );

    // Update the label with the new info.
    ui->layerToRenderLabel->setText( QString::asprintf("Layer To Render : %d", layerToRender ) );
}

void DOFRenderer::on_drawDepthCheckbox_toggled(bool checked)
{
    ui->openGLWidget->setDrawDepth( checked );
}

void DOFRenderer::on_debugColorCheckbox_toggled(bool checked)
{
    ui->openGLWidget->setDebugColor( checked );
}

#ifndef POPUPHELP_H
#define POPUPHELP_H

#include <QDialog>

namespace Ui {
class PopUpHelp;
}

/**
 * Class corresponding to the UI of the windows "Help".
 */
class PopUpHelp : public QDialog
{
    Q_OBJECT

public:
    /**
     * Default constructor.
     * @param parent Parent of the widget if there is.
     */
    explicit PopUpHelp(QWidget *parent = 0);

    /** Destructor. */
    ~PopUpHelp();

private:
    /** UI handle of the Help window. */
    Ui::PopUpHelp *ui;
};

#endif // POPUPHELP_H

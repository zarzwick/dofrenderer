#include "popuphelp.h"
#include "ui_popuphelp.h"

PopUpHelp::PopUpHelp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopUpHelp)
{
    ui->setupUi(this);
}

PopUpHelp::~PopUpHelp()
{
    delete ui;
}

#ifndef POPUPABOUT_H
#define POPUPABOUT_H

#include <QDialog>

namespace Ui {
class PopUpAbout;
}

/**
 * Class corresponding to the UI of the window "About".
 */
class PopUpAbout : public QDialog
{
    Q_OBJECT

public:
    /**
     * Default constructor.
     * @param parent Parent of the widget if there is.
     */
    explicit PopUpAbout(QWidget *parent = 0);

    /** Destructor. */
    ~PopUpAbout();

private:
    /** UI handle of the About window. */
    Ui::PopUpAbout *ui;
};

#endif // POPUPABOUT_H

#include "popupabout.h"
#include "ui_popupabout.h"

PopUpAbout::PopUpAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopUpAbout)
{
    ui->setupUi(this);
}

PopUpAbout::~PopUpAbout()
{
    delete ui;
}

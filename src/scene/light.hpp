#ifndef M1_DOFRDR_LIGHT_H
#define M1_DOFRDR_LIGHT_H

#include "../opengl.hpp"

#include <QtGui/QOpenGLShaderProgram>
#include <glm/glm.hpp>

/**
 * Light. Base class for possibles alternatives (Spot, Cone, Textured light...)
 */
struct Light {
    
public:
    /** Constructor */
    Light( glm::vec3 const& coords, glm::vec3 const& col, float i )
        : _coords{ coords }, _col{ col }, _intensity{ i }
    {
        QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
        gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );
    }
    
    /** Prepare the uniforms */
    void prepare( const QOpenGLShaderProgram& prog, unsigned int i );
    
public:
    /** Coordinates. */
    glm::vec3 _coords;
    
    /** Light color */
    glm::vec3 _col;
    
    /** Light source caracteristics */
    float _intensity;
    
private:
    QOpenGLFunctions_4_1_Core* gl;
    
}; // end Light;


#endif

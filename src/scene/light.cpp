#include "light.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <string>

void Light::prepare( const QOpenGLShaderProgram& prog, unsigned int i )
{
    GLuint pos;
    std::string locationName;
    
    
    locationName = "lights[" + std::to_string(i) + "].coords";
    pos = prog.uniformLocation( locationName.c_str() );
    gl->glUniform3fv( pos, 1, glm::value_ptr( _coords ) );
    
    locationName = "lights[" + std::to_string(i) + "].col";
    pos = prog.uniformLocation( locationName.c_str() );
    gl->glUniform3fv( pos, 1, glm::value_ptr( _col ) );
    
    locationName = "lights[" + std::to_string(i) + "].intensity";
    pos = prog.uniformLocation( locationName.c_str() );
    gl->glUniform1f( pos, _intensity );
}

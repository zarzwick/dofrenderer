#include "mesh.hpp"

#include <QOpenGLFunctions_4_1_Core>

#include <iostream>

using namespace std;


Mesh::Mesh( const std::vector<Vertex>&  vertices,
            const std::vector<GLuint>&  indices,
            const Material mat )
    : _vertices { vertices }
    , _indices  { indices }
    , _mat { mat }
    , _vao { 0 }
    , _vbo { 0 }
    , _ebo { 0 }
{
    QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
    gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );
}


Mesh::~Mesh()
{
    gl->glDeleteBuffers( 1, &_ebo );
    gl->glDeleteBuffers( 1, &_vbo );
    gl->glDeleteVertexArrays( 1, &_vao );
}


void Mesh::draw( QOpenGLShaderProgram* shader )
{
    // Bind the diffuse texture.
    // NOTE: Again, for the sake of simplicity there is one single texture, used by default
    // as a diffuse texture.
    if ( _mat._textured != 0 )
    {
        //_mat._texId
    /*
        glAssert( gl->glActiveTexture(GL_TEXTURE0) );
        glAssert( gl->glUniform1i(gl->glGetUniformLocation(shader->programId(), "diffuseTex_0"), 0) );
        glAssert( gl->glBindTexture(GL_TEXTURE_2D, _textures[0]._id) );
    */
    }

    if ( shader != nullptr )
        shader->bind();

    // Finally draw the mesh
    glAssert( gl->glBindVertexArray( _vao ) );
    glAssert( gl->glDrawElements( GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, (GLvoid*) 0 ) );
    glAssert( gl->glBindVertexArray( 0 ) );

    if ( shader != nullptr )
        shader->release();

    glAssert( gl->glBindTexture(GL_TEXTURE_2D, 0) );
}


void Mesh::init()
{
    // Create buffers
    glAssert( gl->glGenVertexArrays( 1, &_vao ) );
    glAssert( gl->glGenBuffers( 1, &_vbo ) );
    glAssert( gl->glGenBuffers( 1, &_ebo ) );


    glAssert( gl->glBindVertexArray( _vao ) );


    // Bufferize vertex data as static (meshes never change, here)
    glAssert( gl->glBindBuffer( GL_ARRAY_BUFFER, _vbo ) );
    glAssert( gl->glBufferData( GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex),
                     &_vertices[0], GL_STATIC_DRAW ) );
    
    
    // Store theses vertices' indices
    glAssert( gl->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, _ebo ) );
    glAssert( gl->glBufferData( GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(GLuint),
                     &_indices[0], GL_STATIC_DRAW ) );


    // Then describe the data layout.
    // It actually matches the struct layout.

    // Positions
    glAssert( gl->glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, _coords) ) );
    glAssert( gl->glEnableVertexAttribArray( 0 ) );

    // Normals
    glAssert( gl->glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, _normal) ) );
    glAssert( gl->glEnableVertexAttribArray( 1 ) );

    // Textures coords
    glAssert( gl->glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, _texCoords) ) );
    glAssert( gl->glEnableVertexAttribArray( 2 ) );

    // Unbind (do we ?)
    glAssert( gl->glBindVertexArray( 0 ) );
    glAssert( gl->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 ) );
    glAssert( gl->glBindBuffer( GL_ARRAY_BUFFER, 0 ) );

    // Generate the bounding box
    bbox();
}


void Mesh::bbox()
{
    _bbox._min = _vertices[0]._coords;
    _bbox._max = _vertices[0]._coords;

    for ( uint i = 1; i < _vertices.size(); ++ i )
    {
        if ( _bbox._min.x > _vertices[i]._coords.x )
            _bbox._min.x = _vertices[i]._coords.x;

        if ( _bbox._min.y > _vertices[i]._coords.y )
            _bbox._min.y = _vertices[i]._coords.y;

        if ( _bbox._min.z > _vertices[i]._coords.z )
            _bbox._min.z = _vertices[i]._coords.z;

        if ( _bbox._max.x < _vertices[i]._coords.x )
            _bbox._max.x = _vertices[i]._coords.x;

        if ( _bbox._max.y < _vertices[i]._coords.y )
            _bbox._max.y = _vertices[i]._coords.y;

        if ( _bbox._max.z < _vertices[i]._coords.z )
            _bbox._max.z = _vertices[i]._coords.z;
    }

    // A clever man told me, after I was done with this, that it would have been way simpler
    // using min and max. Indeed. I was for sure very tired to forget such an obvious thing.
}


#include "material.hpp"

#include <glm/gtc/type_ptr.hpp>

void Material::prepare( const QOpenGLShaderProgram* prog )
{
    GLuint pos;
    
    pos = prog->uniformLocation("mat.colDiffuse");
    gl->glUniform3fv( pos, 1, glm::value_ptr( _colDiffuse ) );
    
    pos = prog->uniformLocation("mat.colSpecular");
    gl->glUniform3fv( pos, 1, glm::value_ptr( _colSpecular ) );
    
    pos = prog->uniformLocation("mat.roughness");
    gl->glUniform1f( pos, _roughness );

    pos = prog->uniformLocation("mat.textured");
    gl->glUniform1i( pos, _textured );
}


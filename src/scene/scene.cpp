#include "scene.hpp"

using namespace std;


void Scene::bbox()
{
    _bbox = _models[0]._bbox;

    for ( uint i = 1; i < _models.size(); ++ i )
        _bbox = _bbox + _models[i]._bbox;
}


void Scene::load( const string path )
{
    int postProcess = aiProcess_Triangulate | aiProcess_FlipUVs
                    | aiProcess_JoinIdenticalVertices | aiProcess_GenNormals;

    // Load the scene from the file
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile( path, postProcess );

    auto lastSlash = path.find_last_of( '/' );
    string directory = path.substr( 0, lastSlash );

    // Check import status
    if ( (scene == nullptr) ||
         (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) ||
         (scene->mRootNode == nullptr) )
    {
        cerr << "[assimp] Failed to import file " << path << endl;
        return;
    }

    // Process every direct root's child.
    for ( uint i = 0; i < scene->mRootNode->mNumChildren; ++i )
        processModel( scene->mRootNode->mChildren[i], scene, directory );
}


void Scene::processModel( aiNode* node, const aiScene* scene, string dir )
{
    _models.push_back( Model {} );
    Model& m = _models.back();

    // Process all the node's meshes (if any)
    for ( uint i = 0; i < node->mNumMeshes; i++ )
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        m.appendMesh( mesh, scene, _textures, dir );
    }

    m._name = string( node->mName.C_Str() );
    m.bbox();
}


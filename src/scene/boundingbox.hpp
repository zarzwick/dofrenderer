#ifndef M1_DOFRDR_BBOX_H
#define M1_DOFRDR_BBOX_H

#include <glm/glm.hpp>

/** Axis-aligned bounding box. */
struct BoundingBox {

    glm::vec3 _min;
    glm::vec3 _max;

    BoundingBox() = default;
    BoundingBox( glm::vec3 min, glm::vec3 max );

    /**
     * Union operator. Takes two bounding boxes and make a big one.
     */
    friend BoundingBox operator+( BoundingBox a, BoundingBox b );

    /**
     * Return the approximation of a transformed bounding-box.
     * @brief Actually computes the bounding box of the transformed points of the original aa bbox.
     */
    BoundingBox transform( glm::mat4 transformation );

};

#endif


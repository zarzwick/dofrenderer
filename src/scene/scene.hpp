#ifndef M1_DOFRDR_SCENE_H
#define M1_DOFRDR_SCENE_H

#include "model.hpp"
#include "light.hpp"
#include "material.hpp"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <vector>
#include <memory>

#include <QOpenGLTexture>


/**
 * 3D scene to be rendered. Or not, we never know...
 * 
 * The struct contains Models (which contain Meshes) and Lights.
 */
struct Scene {
    
    /** Regenerate bounding box */
    void bbox();

    /**
     * Append assets from 'path' to the scene.
     * NOTE: This "importer" is really primitive and is tested only on Wavefront obj. files,
     * as it has the sole purpose of retrieving simple meshes for demonstration.
     */
    void load( const std::string path );

private:

    /**
     * Process one assimp's Node, assumed here as a set of mesh (bad assumption, but close enough).
     */
    void processModel( aiNode* node, const aiScene* scene, std::string dir );


public:

    /** 3D objects (aka models) */
    std::vector<Model> _models;
    
    /** Scene lights */
    std::vector<Light> _lights;

    /** Scene textures (referenced by Meshes' embedded Materials) */
    std::vector<std::shared_ptr<QOpenGLTexture>> _textures;

    /** Bounding box */
    BoundingBox _bbox;

}; // end Scene


#endif

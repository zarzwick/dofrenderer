#ifndef M1_DOFRDR_GEOMETRY_MESH_H
#define M1_DOFRDR_GEOMETRY_MESH_H

#include "../opengl.hpp"
#include "material.hpp"
#include "boundingbox.hpp"

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

#include <glm/glm.hpp>
#include <vector>


struct Vertex;


/**
 * Mesh object.
 * Basically represents a vertex array with VBO, VAO ans EBO.
 */
class Mesh {
public:
    /**
     * Constructor.
     * @param vertices Array of vertices defining the mesh
     * @param indices Indices qualifying vertices
     */
    Mesh( const std::vector<Vertex>&  vertices,
          const std::vector<GLuint>&  indices,
          const Material mat );
    
    virtual ~Mesh();
    
    /**
     * Draw function.
     * @param shader Shader to bound. Null if done externally.
     */
    void draw( QOpenGLShaderProgram* shader = nullptr );

    /**
     * Set up the mesh, creating VBOs and all.
     */
    void init();

private:
    /**
     * Generate the object-space bounding box in member _bbox.
     */
    void bbox();

public:
    std::vector<Vertex>         _vertices;
    std::vector<GLuint>         _indices;

    Material _mat;

    BoundingBox _bbox;

private:
    GLuint _vao;
    GLuint _vbo;
    GLuint _ebo;
    
    QOpenGLFunctions_4_1_Core* gl;
    
}; // end Mesh



/**
 * Base class for a simple vertex.
 */
struct Vertex {

    glm::vec3   _coords;
    glm::vec3   _normal;

    glm::vec2   _texCoords;

}; // end Vertex


#endif


#ifndef M1_DOFRDR_MATERIAL_H
#define M1_DOFRDR_MATERIAL_H

#include "../opengl.hpp"

#include <QtGui/QOpenGLShaderProgram>
#include <glm/glm.hpp>


/**
 * Material type used to compute Cook-Torrance BRDF.
 */
struct Material {

public:
    /** Constructor */
    Material( glm::vec3 diffuse = {.8f, .8f, .8f}, glm::vec3 spec = {1.f,1.f,1.f}, float r = .7f )
        : _colDiffuse { diffuse }
        , _colSpecular { spec }
        , _roughness { r }
        , _textured { 0 }
    {
        QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
        gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );
    }
    
    /** Function enabling simple glUniform settings */
    void prepare( QOpenGLShaderProgram const* prog );

public:
    /** Diffuse colour */
    glm::vec3 _colDiffuse;

    /** Specular colour */
    glm::vec3 _colSpecular;

    /** Material's roughness */
    float _roughness;

    /** Uage of texture */
    int _textured;
    
    /** Texture ID (if any) */
    int _texId;

private:
    QOpenGLFunctions_4_1_Core* gl;

}; // end Material


#endif

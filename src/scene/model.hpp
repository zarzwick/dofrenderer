#ifndef M1_DOFRDR_MODEL_H
#define M1_DOFRDR_MODEL_H

#include "../opengl.hpp"
#include "mesh.hpp"
#include "material.hpp"
#include "boundingbox.hpp"

#include <vector>
#include <memory>
#include <assimp/scene.h>


/**
 * Model class. Respresents a group of meshes, and provide import function.
 */
class Model {
public:

    Model()
        : _transform{ 1.f }
    {
    }

    /**
     * Constructor.
     */
    Model( const std::vector<Vertex>& vertices,
           const std::vector<GLuint>& indices )
        : _transform { 1.f }
    {
        _meshes.emplace_back( vertices, indices, Material {} );
        _meshes.back().init();
        this->bbox();
    }


    /**
     * Draw the complete Model. (Basically loop over the Meshes)
     */
    void draw( QOpenGLShaderProgram* shader, std::vector< std::shared_ptr<QOpenGLTexture> >& textures );
    
    
    /**
     * Update transformation.
     * @param loc Location in world.
     * @param rot Rotation axis.
     * @param scal Scaling factor.
     */
    void transform( glm::vec3 loc, glm::vec3 rot, glm::vec3 scal );


    /**
     * Regenerate object-aligned bounding box of the model.
     */
    void bbox();
    

    /**
     * Append a Mesh to the model.
     */
    void appendMesh( aiMesh* mesh, const aiScene* scene,
                     std::vector< std::shared_ptr<QOpenGLTexture> >& textures, std::string dir );


public:
    glm::mat4 _transform;               /** Transformation matrix */

    std::string _name;                  /** Model name... */

    BoundingBox _bbox;                  /** Bounding box */

private:
    std::vector<Mesh> _meshes;          /** Mesh container */

}; // end Model


#endif

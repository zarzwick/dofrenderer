#include "model.hpp"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <iostream>
#include <cmath>

#include <glm/gtc/matrix_transform.hpp>

#include <QString>
#include <QImage>


// A huge thank to learnopengl.com tutorial, among others.


using namespace std;


void Model::draw( QOpenGLShaderProgram* shader, vector< shared_ptr<QOpenGLTexture> >& textures )
{
    // Draw every mesh.
    for ( auto& mesh : _meshes )
    {
        mesh._mat.prepare( shader );

        if ( mesh._mat._textured )
            textures[mesh._mat._texId]->bind();

        mesh.draw();

        if ( mesh._mat._textured )
            textures[mesh._mat._texId]->release();
    }
}


void Model::transform( glm::vec3 loc, glm::vec3 rot, glm::vec3 scal )
{
    // http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToAngle/
    float angle = 2 * acos( cos(rot.x)*cos(rot.y)*cos(rot.z) - sin(rot.x)*sin(rot.y)*sin(rot.z) );
    glm::vec3 rotation = {
        sin(rot.x)*sin(rot.y)*cos(rot.z) + cos(rot.x)*cos(rot.y)*sin(rot.z),
        sin(rot.x)*cos(rot.y)*cos(rot.z) + cos(rot.x)*sin(rot.y)*sin(rot.z),
        cos(rot.x)*sin(rot.y)*cos(rot.z) - sin(rot.x)*cos(rot.y)*sin(rot.z),
    };

    _transform = glm::translate( glm::mat4(), loc );
    _transform = glm::rotate( _transform, angle, rotation );
    _transform = glm::scale( _transform, scal );
}


void Model::bbox()
{
    if ( _meshes.size() < 1 )
        return;

    _bbox = _meshes[0]._bbox;

    for ( uint i = 1; i < _meshes.size(); ++ i )
        _bbox = _bbox + _meshes[i]._bbox;
}


void Model::appendMesh( aiMesh* mesh, const aiScene* scene,
                        vector< shared_ptr<QOpenGLTexture> >& textures, string dir )
{
    vector<Vertex>  vertices;
    vector<GLuint>  indices;

    vertices.reserve(mesh->mNumVertices);


    // For each vertex
    for ( GLuint i = 0; i < mesh->mNumVertices; i++ )
    {
        Vertex vertex;

        // Process vertex positions and normals
        vertex._coords = glm::vec3( mesh->mVertices[i].x,
                                    mesh->mVertices[i].y,
                                    mesh->mVertices[i].z );

        vertex._normal = glm::vec3( mesh->mNormals[i].x,
                                    mesh->mNormals[i].y,
                                    mesh->mNormals[i].z );

        // And check for texture coordinates
        glm::vec2 texCo;

        if ( mesh->mTextureCoords[0] != nullptr )
        {
            texCo.x = mesh->mTextureCoords[0][i].x;
            texCo.y = mesh->mTextureCoords[0][i].y;
        }
        else
        {
            texCo = { 0.f, 0.f };
        }

        vertex._texCoords = texCo;

        vertices.push_back(vertex);
    }


    // Process indices
    for ( GLuint i = 0; i < mesh->mNumFaces; i++ )
    {
        aiFace face = mesh->mFaces[i];

        // The mNumIndices is 3 here, as we set the aiProcess_Triangulate option.
        for ( GLuint j = 0; j < face.mNumIndices; j++ )
            indices.push_back(face.mIndices[j]);
    }


    // Process material
    aiColor3D aiDiffCol( .8f, .8f, .8f );
    aiColor3D aiSpecCol( 1.f, 1.f, 1.f );
    float shininess = 0.0;

    aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
    if ( mat != nullptr )
    {
        mat->Get( AI_MATKEY_SHININESS, shininess );
        mat->Get( AI_MATKEY_COLOR_DIFFUSE, aiDiffCol );
        mat->Get( AI_MATKEY_COLOR_SPECULAR, aiSpecCol );
    }

    glm::vec3 diffCol { aiDiffCol.r, aiDiffCol.g, aiDiffCol.b };
    glm::vec3 specCol { aiSpecCol.r, aiSpecCol.g, aiSpecCol.b };

    // Convert from Phong to Beckmann
    shininess = sqrt( 2 / (shininess + 2) );

    _meshes.emplace_back( vertices, indices, Material {diffCol, specCol, shininess} );
    _meshes.back().init();

    // If a texture exists, load it
    if ( mat->GetTextureCount(aiTextureType_DIFFUSE) > 0 )
    {
        cout << "Mesh " << mesh->mName.C_Str() << " has at least 1 texture. Loading..." << endl;

        Material& meshMaterial = _meshes.back()._mat;
        meshMaterial._textured = 1;
        meshMaterial._texId = textures.size();

        aiString aiTexturePath;
        mat->GetTexture( aiTextureType_DIFFUSE, 0, &aiTexturePath);

        QString texturePath = QString::asprintf( "%s/%s", dir.c_str(), aiTexturePath.C_Str() );
        //cout << (const char*)texturePath.toLocal8Bit() << endl;
        textures.emplace_back( new QOpenGLTexture(QImage(texturePath).mirrored()) );
        textures.back()->setMinificationFilter( QOpenGLTexture::LinearMipMapLinear );
        textures.back()->setMagnificationFilter( QOpenGLTexture::Linear );
    }

}


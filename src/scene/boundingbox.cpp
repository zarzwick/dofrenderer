#include "boundingbox.hpp"
#include <utility>

using namespace glm;


BoundingBox::BoundingBox( vec3 min, vec3 max )
    : _min{ min }
    , _max{ max }
{}


BoundingBox operator+( BoundingBox a, BoundingBox b )
{
    BoundingBox u;
    u._min.x = ( a._min.x < b._min.x ) ? a._min.x : b._min.x;
    u._min.y = ( a._min.y < b._min.y ) ? a._min.y : b._min.y;
    u._min.z = ( a._min.z < b._min.z ) ? a._min.z : b._min.z;
    u._max.x = ( a._max.x > b._max.x ) ? a._max.x : b._max.x;
    u._max.y = ( a._max.y > b._max.y ) ? a._max.y : b._max.y;
    u._max.z = ( a._max.z > b._max.z ) ? a._max.z : b._max.z;
    return u;
}


BoundingBox BoundingBox::transform( mat4 transformation )
{
    vec3 p = vec3( vec4(_min, 0) * transformation );
    BoundingBox b { p, p };

    for ( uint i = 1; i < 8; ++ i )
    {
        // Compute a corner
        p.x = (i & 1) ? _max.x : _min.x;
        p.y = (i & 2) ? _max.y : _min.y;
        p.z = (i & 4) ? _max.z : _min.z;
        p = vec3( vec4(p, 0) * transformation );

        // Get new min and max values
        b._min.x = ( p.x < b._min.x ) ? p.x : b._min.x;
        b._min.y = ( p.y < b._min.y ) ? p.y : b._min.y;
        b._min.z = ( p.y < b._min.z ) ? p.z : b._min.z;
        b._max.x = ( p.x < b._max.x ) ? p.x : b._max.x;
        b._max.y = ( p.y < b._max.y ) ? p.y : b._max.y;
        b._max.z = ( p.y < b._max.z ) ? p.z : b._max.z;
    }

    return b;
}


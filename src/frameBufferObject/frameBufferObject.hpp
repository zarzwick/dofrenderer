#ifndef M1_DOFRDR_FRAMEBUFFEROBJECT_H
#define M1_DOFRDR_FRAMEBUFFEROBJECT_H

#include "../opengl.hpp"

#include <QOpenGLShaderProgram>

/**
 * Interface for OpenGL Framebuffer.
 * Allow to render a scene on one or several textures
 * Then render these textures to the screen.
 * NDC = Normalized Device Coordinates.
 */
class FrameBufferObject
{
public:
    /**
     * Initialize the Framebuffer, must be called with OpenGL context ownship.
     * ( initializeGL of Viewport is great ).
     * @param width Width of the texture(s) to render on it.
     * @param height Height of the texture(s) to render on it.
     * @param layerCount Max count of layer in the framebuffer.
     */
    explicit FrameBufferObject( uint width, uint height, uint layerCount );

    /** Destructor. */
    ~FrameBufferObject();
    
    /**
     * Render to the screen in NDC the texture(s).
     * @param layerToRender ID of the layer to render.
     * @param isDepth Draw depth texture instead of color one.
     */
    void render( uint layerToRender, bool isDepth = false );

    /**
     * Bind the framebuffer to the OpenGL Context.
     * All drawcall after this one will draw object on the
     * Framebuffer texture(s).
     */
    void bind( bool checkFBO = true );

    /** Reset the framebuffer to the Qt default one. */
    void unbind();

    /** Bind the fbo shader, for one-layer mode display. */
    void bindShader( int layer = 0, bool depth = false );

    /** Unbind the fbo shader. */
    void unbindShader();

    /** Check if the current framebuffer is complete. */
    bool checkStatus();

    /**
     * Resize the FBO textures. To be called when OpenGL widget is resized.
     * @param width New width to apply to the textures.
     * @param height New height to apply to the textures.
     */
    void resize( uint width, uint height );

    /**
     * Set the count of layer available for the layered rendering.
     * @param layerCount New count of layer.
     * @return Count of layer clamped if it was greater than the limit MAX_LAYER_COUNT
     */
    uint setLayerCount( uint layerCount );

    /**
     * Retrieve the count of layer available for the layered rendering.
     * @return Count of layer.
     */
    uint getLayerCount() const;

    /**
     * Return the color texture array.
     */
    GLuint getColorTexture() const;

    /**
     * Return the depth texture array.
     */
    GLuint getDepthTexture() const;

    /**
     * Get width.
     */
    uint getWidth() const;

    /**
     * Get height.
     */
    uint getHeight() const;

    /**
     * Active a specific layer to use as target.
     * Other layers will not be used to render the scene.
     *
     * @param layerID ID of the layer to use as target. -1 active all layers.
     * @param bindFBO If true bind and unbind the fbo, otherwise does not bind/unbind the fbo.
     */
    void setTargetLayer( int layerID, bool bindFBO );

private:
    /** Load and compile shader to render the texture(s) in NDC. */
    void initializeShader();

    /** Initialize OpenGL framebuffer and create texture(s). */
    void initializeFBO();

    /**
     * Create textures arrays (Color and Depth) for FBO.
     */
    void createTextures();
    /**
     * Destroy textures arrays (Color and Depth).
     */
    void destroyTextures();
    /**
     * Link textures arrays (Color and Depth) to the FBO.
     */
    void linkTextures();

public:
    /** Maximum count of layer allowed to use. */
    static constexpr uint MAX_LAYER_COUNT = 32;

private:
    /** Reference to Qt OpenGL functions. Not owned. */
    QOpenGLFunctions_4_1_Core* _gl;

    /** OpenGL ID for the framebuffer. */
    GLuint _fbo;
    /** OpenGL ID for color texture bound to framebuffer. */

    GLuint _colorTexture;
    /** OpenGL ID for depth texture bound to framebuffer. */

    GLuint _depthTexture;

    /** Uniform ID of the int corresponding to the layer ID to be render. */
    GLuint _quadLayerToDisplay;

    /**
     * Uniform ID of the boolean corresponding to which texture will rendered,
     * if true it will be the depth texture, otherwise it will be the color texture.
     */
    GLuint _quadIsDepth;

    /** Shader to render the texture(s) in NDC on a quad. */
    QOpenGLShaderProgram _shader;

    /** Width of texture(s). */
    uint _width;

    /** Height of texture(s). */
    uint _height;

    /** Count of layer in framebuffer. */
    uint _layerCount;

};

#endif // M1_DOFRDR_FRAMEBUFFEROBJECT_H

#include "screenmesh.hpp"


ScreenMesh::ScreenMesh()
    : Mesh{ // Really beautiful mesh creation.
        { { {-1,-1, 0}, { 0, 0, 1}, {0,0} },
          { { 1,-1, 0}, { 0, 0, 1}, {1,0} },
          { { 1, 1, 0}, { 0, 0, 1}, {1,1} },
          { {-1, 1, 0}, { 0, 0, 1}, {0,1} } },
        { 3, 0, 1, 3, 1, 2 }, {} }
{
    // Nothing !
}


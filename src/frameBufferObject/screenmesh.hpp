#ifndef M1_DOFRDR_SCREENMESH_HPP
#define M1_DOFRDR_SCREENMESH_HPP

#include "../scene/mesh.hpp"
#include <QOpenGLShaderProgram>


/**
 * Quad mesh specifically targeted at rendering onscreen.
 */
class ScreenMesh : public Mesh {

public:
    /** Create a mesh with 4 appropriate vertices. */
    ScreenMesh();

}; // end ScreenMesh


#endif


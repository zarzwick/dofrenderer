#include "frameBufferObject.hpp"

//#include "../scene/mesh.hpp"

#include <QOpenGLFunctions_4_1_Core>
#include <QOpenGLFunctions>

#include <qmath.h>
#include <fstream>


FrameBufferObject::FrameBufferObject(uint width, uint height , uint layerCount)
    :  _width { width }
    , _height { height }
    , _layerCount { (layerCount > MAX_LAYER_COUNT) ? MAX_LAYER_COUNT : layerCount }
{
    QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
    _gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );
    
    // Shader.
    initializeShader();

    // FBO
    initializeFBO();
}


FrameBufferObject::~FrameBufferObject()
{
    destroyTextures();
    glAssert( _gl->glDeleteFramebuffers( 1, &_fbo ) );
}


void FrameBufferObject::initializeFBO()
{
    glAssert( _gl->glGenFramebuffers( 1, &_fbo ) );
    bind( false );

    createTextures();
    linkTextures();

    if( checkStatus() )
        std::cerr << "FrameBufferObject::FrameBufferObject(int width, int height) : Failed to create a complete framebuffer\n";

    unbind();
}


void FrameBufferObject::initializeShader()
{
    _shader.addShaderFromSourceFile( QOpenGLShader::Fragment, "../shaders/fbo.glsl.frag" );
    _shader.addShaderFromSourceFile( QOpenGLShader::Vertex, "../shaders/fbo.glsl.vert" );

    _shader.link();

    if( !_shader.isLinked() )
        std::cerr << "FrameBufferObject::initializeShader : Failed to link shader.\n";

    _quadLayerToDisplay = _gl->glGetUniformLocation(_shader.programId(), "layerToDisplay");
    _quadIsDepth = _gl->glGetUniformLocation(_shader.programId(), "isDepth");
}


void FrameBufferObject::createTextures()
{
    // Color Texture Creation.
    glAssert( _gl->glGenTextures( 1, &_colorTexture ) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _colorTexture ) );
    glAssert( _gl->glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_RGB, _width, _height, _layerCount, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL ) );
    glAssert( _gl->glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
    glAssert( _gl->glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );

    // Depth Texture Creation.
    glAssert( _gl->glGenTextures( 1, &_depthTexture ) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _depthTexture ) );
    glAssert( _gl->glTexImage3D( GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH_COMPONENT, _width, _height, _layerCount, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL ) );
    glAssert( _gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    glAssert( _gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    glAssert( _gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    glAssert( _gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
    glAssert( _gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL) );
    glAssert( _gl->glTexParameteri (GL_TEXTURE_2D_ARRAY, GL_TEXTURE_COMPARE_MODE, GL_NONE) );

    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, 0 ) );
}


void FrameBufferObject::destroyTextures()
{
    glAssert( _gl->glDeleteTextures( 1, &_colorTexture ) );
    glAssert( _gl->glDeleteTextures( 1, &_depthTexture ) );
}


void FrameBufferObject::linkTextures()
{
    bind( false );

    glAssert( _gl->glFramebufferTexture( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _colorTexture, 0 ) );
    glAssert( _gl->glFramebufferTexture( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexture, 0 ) );

    unbind();
}


void FrameBufferObject::bind(bool checkFBO)
{
    glAssert( _gl->glBindFramebuffer( GL_FRAMEBUFFER, _fbo ) );

    if( checkFBO && checkStatus() )
    {
        std::cerr << "FrameBufferObject::bind() : Failed to bind framebuffer\n";
        return;
    }
}


void FrameBufferObject::unbind()
{
    QOpenGLContext* current = QOpenGLContext::currentContext();
    if( !current )
    {
        std::cerr << "FrameBufferObject::unbind() : Context openGL invalid\n";
        return;
    }

    glAssert( _gl->glBindFramebuffer( GL_FRAMEBUFFER, current->defaultFramebufferObject() ) );

    if( checkStatus() )
    {
        std::cerr << "FrameBufferObject::unbind() : Framebuffer is in an invalid state.\n";
        return;
    }
}


void FrameBufferObject::bindShader( int layer, bool depth )
{
    _shader.bind();

    glAssert( _gl->glUniform1ui(_quadIsDepth, static_cast<GLboolean>(depth)) );
    glAssert( _gl->glUniform1i(_quadLayerToDisplay, layer) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE0) );

    if ( depth )
    {
        glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _depthTexture ) );
    }
    else
    {
        glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _colorTexture ) );
    }
}


void FrameBufferObject::unbindShader()
{
    glAssert( _gl->glActiveTexture( GL_TEXTURE0 ) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, 0 ) );
    _shader.release();
}


bool FrameBufferObject::checkStatus()
{
    return (_gl->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE);
}


void FrameBufferObject::resize( uint width, uint height )
{
    if( _width == width && _height == height )
        return;

    _width = width;
    _height = height;

    if( checkStatus() )
    {
        std::cerr << "FrameBufferObject::resize() : Framebuffer is in an invalid state.\n";
        return;
    }

    // Update textures with the new size.
    destroyTextures();
    createTextures();
    linkTextures();
}


uint FrameBufferObject::setLayerCount( uint layerCount )
{
    if( _layerCount == layerCount )
         return _layerCount;

    if( checkStatus() )
    {
        std::cerr << "FrameBufferObject::setLayerCount() : Framebuffer is in an invalid state.\n";
        return _layerCount;
    }

    if( layerCount <= 0 )
    {
        std::cerr << "FrameBufferObject::setLayerCount() : FBO need at greater than 0.\n";
        _layerCount = 1;
    }

    if( layerCount > MAX_LAYER_COUNT )
    {
        std::cerr << "FrameBufferObject::setLayerCount() : Layer count must be lower or equal to " << MAX_LAYER_COUNT << ".\n";
        _layerCount = MAX_LAYER_COUNT;
    }

    _layerCount = layerCount;

    // Update the textures with the new layer count.
    destroyTextures();
    createTextures();
    linkTextures();

    return _layerCount;
}


uint FrameBufferObject::getLayerCount() const
{
    return _layerCount;
}


GLuint FrameBufferObject::getColorTexture() const
{
    return _colorTexture;
}


GLuint FrameBufferObject::getDepthTexture() const
{
    return _depthTexture;
}

void FrameBufferObject::setTargetLayer(int layerID , bool bindFBO)
{
    if( bindFBO )
        bind( true );

    if( layerID >= 0 )
    {
        glAssert( _gl->glFramebufferTextureLayer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _colorTexture, 0, layerID ) );
        glAssert( _gl->glFramebufferTextureLayer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexture, 0, layerID ) );
    }
    else
    {
        glAssert( _gl->glFramebufferTexture( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _colorTexture, 0 ) );
        glAssert( _gl->glFramebufferTexture( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexture, 0 ) );
    }

    if( bindFBO )
        unbind();
}


GLuint FrameBufferObject::getWidth() const
{
    return _width;
}


GLuint FrameBufferObject::getHeight() const
{
    return _height;
}


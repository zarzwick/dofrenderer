#include "decomposition.hpp"

#include "../camera/paraphysique.hpp"
#include "../frameBufferObject/frameBufferObject.hpp"
#include "../frameBufferObject/screenmesh.hpp"

#include <iostream>


Decomposition::Decomposition()
{
    QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
    _gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );

    initializeShader();
}


double Decomposition::getDelta( const double& nearCOC, const double& farCOC, const double& layerBudget )
{
    // Find step for each layer id (0 to (layerCount - 1))
    const double& delta = qAbs(farCOC - nearCOC) / (layerBudget - 1);

    return delta;
}

double Decomposition::getDelta() const
{
    return _delta;
}

double Decomposition::getCOC( const OpticsSettings& settings, const double& distance )
{
    /*
    // Equation 2. (COC radius in world space). 'R'. The radius is signed.
    const double& focusDistance = settings.getFocusDistance();
    const double& lensRadius = settings.getLensRadius();

    const double& distanceFactor = (focusDistance - distance) / focusDistance;

    return (lensRadius * distanceFactor);
    */

    // Equation 3. (COC radius in image space). 'r'. The radius is signed.
    const double&& focusDistance = settings.getFocusDistance();
    const double&& lensRadius = settings.getLensRadius();
    const double&& focalLength = settings.getFocalLength();

    const double&& distanceFactor = distance - focusDistance;
    const double&& focalFactor = focusDistance - focalLength;

    const double&& COCRadius = (lensRadius * focalLength * distanceFactor) / (focalFactor * distance);

    return COCRadius;
}

double Decomposition::getK( const OpticsSettings& settings )
{
    const double&& EF = settings.getLensRadius() * settings.getFocalLength();
    const double&& dfLessF = settings.getFocusDistance() - settings.getFocalLength();

    return (EF / dfLessF);
}

double Decomposition::getK() const
{
    return _K;
}

double Decomposition::getLeftFactor( const OpticsSettings& settings, const double& K, const double& delta )
{
    const double&& Kdf = K * settings.getFocusDistance();

    return (Kdf / delta);
}

int Decomposition::getLayerID(const OpticsSettings& settings, const double& distance, const double& layerBudget )
{
    // Equation 4

    const double&& minCOC = getCOC( settings, settings.getNearDistance() );
    const double&& maxCOC = getCOC( settings, settings.getFarDistance() );

    const double&& near = settings.getNearDistance();

    const double& distanceChecked = qBound(near, distance, settings.getFarDistance());

    const double&& leftFactor = getLeftFactor( settings, getK( settings ), getDelta( minCOC, maxCOC, layerBudget ) );
    const double&& rightFactor = (1.0 / near) - (1.0 / distanceChecked);

    return (leftFactor * rightFactor);
}

QOpenGLShaderProgram& Decomposition::GetShaderProgram()
{
    return _shader;
}

void Decomposition::bindShader(const OpticsSettings& settings, const glm::vec3& lensPosition, const double& layerBudget )
{
    if( !_shader.bind() )
        std::cerr << "Decomposition::bindShader : Failed to bind shader.\n";

    _isShaderBound = true;

    // Setup decomposition settings in shader.
    const double&& minCOC = getCOC( settings, settings.getNearDistance() );
    const double&& maxCOC = getCOC( settings, settings.getFarDistance() );
    _delta = getDelta( minCOC, maxCOC, layerBudget );
    _K = getK(settings);

    // Left factor = (K * df) / delta
    const double&& leftFactor = getLeftFactor( settings, _K, _delta );

    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::NearDistance], static_cast<float>(settings.getNearDistance()) ) );
    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::FarDistance], static_cast<float>(settings.getFarDistance()) ) );
    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::LeftFactor], static_cast<float>(leftFactor) ) );
    glAssert( _gl->glUniform3fv( _shaderSettings[ShaderSettings::LensPosition], 1, glm::value_ptr(lensPosition) ) );
}

void Decomposition::unbindShader()
{
    if( _isShaderBound )
    {
        _isShaderBound = false;
        _shader.release();
    }
}

bool Decomposition::isShaderBound() const
{
    return _isShaderBound;
}

GLuint Decomposition::getShaderSettingPosition(Decomposition::ShaderSettings setting)
{
    if( setting >= 0 && setting < ShaderSettings::SettingsCount )
    {
        return _shaderSettings[setting];
    }
    else
    {
        std::cerr << "Decomposition::getShaderSettingPosition : Setting asked unknown.\n";
        return 0;
    }
}

double Decomposition::meterToMillimeter(const double &meters)
{
    return (meters * 1000.0);
}

double Decomposition::millimeterToMeter(const double &millimeters)
{
    return (millimeters / 1000.0);
}

void Decomposition::initializeShader()
{
    // First pass.
    _isShaderBound = false;

    if( !_shader.addShaderFromSourceFile( QOpenGLShader::Fragment, "../shaders/cooktorr.glsl.frag" ) )
        std::cerr << "Decomposition::initializeShader : Failed to load frag shader. : " << "shaders/cooktorr.glsl.frag" << "\n";

    if( !_shader.addShaderFromSourceFile( QOpenGLShader::Vertex, "../shaders/default.glsl.vert" ) )
        std::cerr << "Decomposition::initializeShader : Failed to load vert shader. : " << "shaders/default.glsl.vert" << "\n";

    if( !_shader.addShaderFromSourceFile( QOpenGLShader::Geometry, "../shaders/decomposition.glsl.geom"  ) )
        std::cerr << "Decomposition::initializeShader : Failed to load geom shader. : " << "shaders/decomposition.glsl.geom" << "\n";

    _shader.link();

    if( !_shader.isLinked() )
        std::cerr << "Decomposition::initializeShader : Failed to link first pass shader.\n";

    _shaderSettings[ShaderSettings::Model] = _shader.uniformLocation( "model" );
    _shaderSettings[ShaderSettings::Normal] = _shader.uniformLocation( "normalTransform" );
    _shaderSettings[ShaderSettings::View] = _shader.uniformLocation( "view" );
    _shaderSettings[ShaderSettings::Projection] = _shader.uniformLocation( "projection" );
    _shaderSettings[ShaderSettings::CameraPosition] = _shader.uniformLocation( "camPos" );
    _shaderSettings[ShaderSettings::LayerCount] = _shader.uniformLocation( "layerCount" );

    _shaderSettings[ShaderSettings::NearDistance] = _shader.uniformLocation( "nearDistance" );
    _shaderSettings[ShaderSettings::FarDistance] = _shader.uniformLocation( "far" );
    _shaderSettings[ShaderSettings::LeftFactor] = _shader.uniformLocation( "leftFactor" );
    _shaderSettings[ShaderSettings::LensPosition] = _shader.uniformLocation( "lensPosition" );
}

QOpenGLShaderProgram& Decomposition::shader()
{
    return _shader;
}


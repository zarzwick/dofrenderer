#include "duplication.hpp"

#include "../camera/paraphysique.hpp"
#include "../frameBufferObject/frameBufferObject.hpp"
#include "../frameBufferObject/screenmesh.hpp"
#include "decomposition.hpp"

#include <iostream>

Duplication::Duplication(uint screenWidth, uint screenHeight, uint layerCount)
{
    QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
    _gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );

    initializeShader();

    _fboResult = new FrameBufferObject( screenWidth, screenHeight, layerCount );
    if( _fboResult == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create layered FBO.\n";

}

Duplication::~Duplication()
{
    if( _fboResult != nullptr )
        delete _fboResult;
}

void Duplication::duplicate( const OpticsSettings& settings, Decomposition* decomposition, FrameBufferObject* FBO, ScreenMesh* FBOMesh, bool debugColor )
{
    if( FBO == nullptr || FBOMesh == nullptr || decomposition == nullptr)
        return;


    // Update the result fbo in case the original fbo settings have changed.
    _fboResult->resize( FBO->getWidth(), FBO->getHeight() );
    _fboResult->setLayerCount( FBO->getLayerCount()  );

    _fboResult->bind();

    _gl->glClearColor( 0.1f, 0.21f, 0.24f, 1.0f );
    _gl->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    _gl->glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    if( !_shader.bind() )
        std::cerr << "Duplication::Duplicate : Failed to bind shader.\n";

    // Left factor = (K * df) / delta
    const double&& leftFactor = decomposition->getLeftFactor( settings, decomposition->getK(), decomposition->getDelta() );

    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::NearDistance], static_cast<float>(settings.getNearDistance()) ) );
    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::FarDistance], static_cast<float>(settings.getFarDistance()) ) );
    glAssert( _gl->glUniform1f( _shaderSettings[ShaderSettings::LeftFactor], static_cast<float>(leftFactor) ) );
    glAssert( _gl->glUniform1i( _shaderSettings[ShaderSettings::LayerCount], _fboResult->getLayerCount() ) );
    glAssert( _gl->glUniform1i( _shaderSettings[ShaderSettings::DebugColor], debugColor ) );

    // Attach images layer of objects rendering.
    glAssert( _gl->glUniform1i(_shaderSettings[FBOColors], 0) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE0) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, FBO->getColorTexture() ) );

    glAssert( _gl->glUniform1i(_shaderSettings[FBODepths], 1) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE1) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, FBO->getDepthTexture() ) );

    // Attach result textures, we will use the previous layer for overlaping processing at each layer second pass.
    glAssert( _gl->glUniform1i(_shaderSettings[DupColors], 2) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE2) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _fboResult->getColorTexture() ) );

    glAssert( _gl->glUniform1i(_shaderSettings[DupDepths], 3) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE3) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, _fboResult->getDepthTexture() ) );

    // Redraw each layer on the result fbo to process duplication and overlaping.
    for( uint layer = 0; layer < FBO->getLayerCount(); layer++ )
    {
        _fboResult->setTargetLayer( layer, false );

        glAssert( _gl->glUniform1i( _shaderSettings[ShaderSettings::CurrentLayer], layer ) );

        FBOMesh->draw();
    }

    _shader.release();
    // Restore default fbo target (all layer active).
    _fboResult->setTargetLayer( -1, false );
    _fboResult->unbind();
}

FrameBufferObject* Duplication::tradeFBO(FrameBufferObject* fboToTrade)
{
    FrameBufferObject* toReturn = _fboResult;
    _fboResult = fboToTrade;

    return toReturn;
}

void Duplication::initializeShader()
{
    if( !_shader.addShaderFromSourceFile( QOpenGLShader::Fragment, "../shaders/duplication.glsl.frag" ) )
        std::cerr << "Decomposition::initializeShader : Failed to load frag shader. : " << "shaders/duplication.glsl.frag" << "\n";

    if( !_shader.addShaderFromSourceFile( QOpenGLShader::Vertex, "../shaders/fbo.glsl.vert" ) )
        std::cerr << "Decomposition::initializeShader : Failed to load vert shader. : " << "shaders/default.glsl.vert" << "\n";

    _shader.link();

    if( !_shader.isLinked() )
        std::cerr << "Decomposition::initializeShader : Failed to link second pass shader.\n";

    _shaderSettings[ShaderSettings::NearDistance] = _shader.uniformLocation( "nearDistance" );
    _shaderSettings[ShaderSettings::FarDistance] = _shader.uniformLocation( "farDistance" );
    _shaderSettings[ShaderSettings::LeftFactor] = _shader.uniformLocation( "leftFactor" );
    _shaderSettings[ShaderSettings::LayerCount] = _shader.uniformLocation( "layerCount" );
    _shaderSettings[ShaderSettings::CurrentLayer] = _shader.uniformLocation( "currentLayer" );

    _shaderSettings[ShaderSettings::FBOColors] = _shader.uniformLocation( "fboColors" );
    _shaderSettings[ShaderSettings::FBODepths] = _shader.uniformLocation( "fboDepths" );
    _shaderSettings[ShaderSettings::DupColors] = _shader.uniformLocation( "duplicationColors" );
    _shaderSettings[ShaderSettings::DupDepths] = _shader.uniformLocation( "duplicationDepths" );

    _shaderSettings[ShaderSettings::DebugColor] = _shader.uniformLocation( "debugColor" );
}

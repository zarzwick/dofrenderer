#ifndef DUPLICATION_H
#define DUPLICATION_H

#include "../opengl.hpp"
#include <QOpenGLShaderProgram>
#include <glm/gtc/type_ptr.hpp>

#include "glm/vec3.hpp"

class OpticsSettings;
class FrameBufferObject;
class ScreenMesh;
class Decomposition;

/**
 * This class will be used to duplication big triangle and process 10% overlaping.
 * The result is stored in a FBO that can be retrieve in exchange of an another.
 */
class Duplication
{
public:
    /** Available uniform in shader settable. */
    enum ShaderSettings
    {
        LayerCount,
        NearDistance,
        FarDistance,
        LeftFactor,

        FBOColors,
        FBODepths,
        DupColors,
        DupDepths,
        CurrentLayer,

        DebugColor,

        SettingsCount
    };

public:
    /**
     * Constructor with properties of the original FBO (the one in the viewport).
     * @param screenWidth
     * @param screenHeight
     * @param layerCount
     */
    Duplication( uint screenWidth, uint screenHeight, uint layerCount );

    /** Destructor */
    ~Duplication();

    /**
     * Duplicate big triangles and process 10% overlaping.
     * Result is stored in the Duplication class fbo.
     *
     * @param settings Optics settings to process layer.
     * @param decomposition Decomposition functionalities to retrieve precalculated layer processing elements.
     * @param FBO Original FBO on which the objects have been rendered. WILL NOT HOLD THE FINAL RESULT.
     * @param FBOMesh A mesh to render the duplication on.
     * @param debugColor Apply debug color (Red : overlaping, Blue : duplication, Green : unchanged).
     */
    void duplicate( const OpticsSettings& settings, Decomposition* decomposition, FrameBufferObject* FBO, ScreenMesh* FBOMesh, bool debugColor );

    /**
     * Give the duplication fbo in exchange of an another fbo to hold the next results.
     * @param fboToTrade FBO to give to duplication.
     * @return FBO previously stored in duplication. This fbo hold the results get with duplicate (if it has been called before).
     */
    FrameBufferObject* tradeFBO(FrameBufferObject* fboToTrade );

private:
    /** Compile the shader and retrieve uniforms positions. */
    void initializeShader();

private:
    /** Reference to Qt OpenGL functions. Not owned. */
    QOpenGLFunctions_4_1_Core* _gl;

    /** Shader progrom (default vertex + duplication frag). */
    QOpenGLShaderProgram _shader;

    /** Uniforms positions in shader. */
    GLuint _shaderSettings[ShaderSettings::SettingsCount];

    /** FBO that will hold the results of duplicate. */
    FrameBufferObject* _fboResult;
};

#endif // DUPLICATION_H

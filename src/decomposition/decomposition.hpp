#ifndef DECOMPOSITION_H
#define DECOMPOSITION_H

#include "../opengl.hpp"
#include <QOpenGLShaderProgram>
#include <glm/gtc/type_ptr.hpp>

#include "glm/vec3.hpp"

class OpticsSettings;
class FrameBufferObject;
class ScreenMesh;

/**
 * This class give tools to use the decomposition shader
 * and process the decomposition in several layer to process the depth of field.
 * It use the equation 4 of the paper "Depth of Field rendering with multiview synthesis.
 * ⌊ ( (K * df) / delta ) * ( (1 / dnear) - (1 / d) ) ⌋
 * df is the focus distance
 * dnear is the minimum distance that we can focus on ( near plane ).
 * d is the distance of the point to process.
 *
 * K is a factor equivalent to (E * F) / (df - F)
 * where E is the radius of the lens and F the focal length.
 *
 * All value are in millimeters.
 * COC = Circle Of Confusion.
 */
class Decomposition
{
public:
    /** Available uniform in shader settable. */
    enum ShaderSettings
    {
        Model,
        Normal,
        View,
        Projection,
        CameraPosition,
        LayerCount,
        NearDistance,
        FarDistance,
        LeftFactor,
        LensPosition,

        SettingsCount
    };

public:
    /** Default constructor. */
    Decomposition();

    /**
     * Delta is the step of the circle of confusion change between each layer.
     * @param nearCOC COC radius in image space (retrieved via getCOC) of near distance.
     * @param farCOC COC radius in image space (retrieved via getCOC) of far distance.
     * @param layerBudget Number of available layer in the FBO.
     * @return Delta factor of the equation.
     */
    double getDelta( const double& nearCOC, const double& farCOC, const double& layerBudget );

    /**
     * Process the radius of the COC in image space (equation 3).
     * @param settings Optics settings to process the depth of field.
     * @param distance Distance to process the COC at.
     * @return The COC radius at the given distance. The radius is signed.
     */
    double getCOC( const OpticsSettings& settings, const double& distance );

    /**
     * Get the K factor of the equation of layer processing.
     * Equivalent to : (E * F) / (df - F).
     * @param settings Optics setting to process depth of field.
     * @return K factor of the equation.
     */
    double getK( const OpticsSettings& settings );

    /**
     * Process the left factor of the layer process. (See class description).
     * That correspond to : (K * df) / delta.
     * @param settings Optics settings to process depth of field.
     * @param K Factor equivalent to (E * F) / (df - F).
     * @param delta Step of circle of confusion for each layer.
     * @return The left factor of the equation.
     */
    double getLeftFactor( const OpticsSettings& settings, const double& K, const double& delta );

    /**
     * Process the same thing that the shader. Debug purpose.
     * @param settings Optics settings to process the depth of field.
     * @param distance (Millimeters) Distance to focus on.
     * @param layerBudget Number of available layer in the FBO.
     * @return Layer corresponding to the depth of field of the point.
     */
    int getLayerID( const OpticsSettings& settings, const double& distance, const double& layerBudget );

    /**
     * Retrieve the Qt shader program.
     * @return Shader program.
     */
    QOpenGLShaderProgram& GetShaderProgram();

    /**
     * Bind the shader (enable it).
     * @param settings Optics setting to send to the shader (To process the depth of field).
     * @param lensPosition Equivalent to the camera position.
     * @param layerBudget Number of available layer in the FBO.
     */
    void bindShader( const OpticsSettings& settings, const glm::vec3& lensPosition, const double& layerBudget );

    /** Unbind (disable) the shader.*/
    void unbindShader();

    /**
     * Is the shader was successfully bound or not.
     * @return True if the shader is bound (enable), false otherwise.
     */
    bool isShaderBound() const;

    /**
     * Get the uniform position of a specific setting in shader.
     * @param setting Setting corresponding the uniform in shader.
     * @return Position of the uniform in the shader.
     */
    GLuint getShaderSettingPosition( ShaderSettings setting );

    /**
     * Convert meter ( 1 world unite ) to millimeter.
     * @param meters World unite to convert to millimeter.
     * @return meter as millimeter.
     */
    static double meterToMillimeter( const double& meters );

    /**
     * Convert distance in millimeters to meter ( world unite ).
     * @param millimeters Distance as millimeters to convert as meter.
     * @return Distance as meters.
     */
    static double millimeterToMeter( const double& millimeters );

    /**
     * Access to the decomposition shader.
     */
    QOpenGLShaderProgram& shader();

    /**
     * Access the precomputed value of delta. Make sure it is up to date.
     */
    double getDelta() const;

    /**
     * Access the precomputed value of K. Make sure it is up to date.
     */
    double getK() const;


private:
    /** Compile the shader and retrieve uniforms positions. */
    void initializeShader();

private:
    /** Reference to Qt OpenGL functions. Not owned. */
    QOpenGLFunctions_4_1_Core* _gl;

    /** Shader is successfully bound or not. */
    bool _isShaderBound;

    /** Shader to override FBO default one to decompose the scene. */
    QOpenGLShaderProgram _shader;

    /** Uniforms positions in shader. */
    GLuint _shaderSettings[ShaderSettings::SettingsCount];

    /** Precomputed value for delta, reset at every bindShader(). */
    double _delta;

    /** Precomputed version of K, reset at every bindShader(). */
    double _K;

}; // Decomposition


#endif // DECOMPOSITION_H

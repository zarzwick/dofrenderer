#ifndef M1_DOFRDR_OPENGL_H
#define M1_DOFRDR_OPENGL_H

// Include the 4.1 core profile API
#include <QtGui/QOpenGLFunctions_4_1_Core>

#if __linux__
    #include <GL/glu.h>
#elif __APPLE__
    #include <OpenGL/glu.h>
#endif

// Include a macro aimed at systematically testing gl error
#include "glassert.h"


// NOTE This header 1) is cool 2) prevents from if __linux__ elif __APPLE__ ...

#endif

#ifndef M1DEMO_GLVIEWPORT_H
#define M1DEMO_GLVIEWPORT_H

#include "opengl.hpp"
#include "camera/cameraFPS.hpp"
#include "camera/paraphysique.hpp"
#include "scene/scene.hpp"
#include "frameBufferObject/screenmesh.hpp"

#include "decomposition/decomposition.hpp"
#include "recomposition/recomposer.hpp"

#include "distribution/poissondisk.hpp"

#include <QOpenGLWidget>
#include <QOpenGLFramebufferObject>

#include <functional>


class QMouseEvent;
class QTimer;
class FrameBufferObject;
class Decomposition;
class Duplication;

/**
 * OpenGL viewport aimed at rendering the main scene.
 * 
 * It subclasses the QtGui/QOpenGLWidget (Qt 5) class and inherits the QOpenGLFunction_...
 * to be able to use glFunctions without prefix.
 */
class GLViewport : public QOpenGLWidget, protected QOpenGLFunctions_4_1_Core
{
    Q_OBJECT
    
public:
    /**
     * Default constructor.
     * @param parent Widget parent. Optional.
     */
    GLViewport ( QWidget* parent );
    /** Destructor. */
    virtual ~GLViewport();

    /**
     * Set the callback called a the end of intializeGL().
     * @param _callback Function to call after the initialization of OpenGL.
     */
    void setUIOnInitializeOpenGLCallback( const std::function<void()>& _callback );

    /**
     * Add a model from file.
     * @param file File to import.
     * @return ID of the new model.
     */
    uint addModel( const GLchar* file );

    /**
     * Remove a model
     * @param model ID of the Model in _models to remove.
     */
    void removeModel( uint model );
    
    /**
     * Remove all the model
     */
    void removeAll();

    /**
     * Apply a tranformation to a model
     * @param model ID of the Model in _models to remove.
     * @param loc Location in world.
     * @param rot Rotation axis.
     * @param scal Scaling factor.
     */
    void transformModel( uint model, glm::vec3 loc, glm::vec3 rot, glm::vec3 scal );
    
    /**
     * Return the list of the models' names.
     * @return Ordered name list (each name's index correspond to the model's one).
     */
    std::list<std::string> getModelsNames() const;
    
    /**
     * Enable or disable wireframed rendering.
     * @param toggle If true render the scene in wireframe, with face filled otherwise.
     */
    void toggleWireframe(bool toggle);
    
    /**
     * Determine which layer of the FBO will be rendered.
     * @param layerToRender ID of the layer to render, must be between 0 and (layer count - 1).
     * @return Layer ID clamped if it was greater than the limit ( layer count - 1 ).
     */
    uint setLayerToRender( uint layerToRender );

    /**
     * Limit imposed for maximum count of layer of the FBO.
     * @return Limit for layer count.
     */
    uint getMaxLayerCount() const;

    /**
     * Set the count of layer available for the FBO.
     * @param layerCount New count of layer. Must be lower than limit fixed (getMaxLayerCount).
     * @return
     */
    uint setLayerCount( uint layerCount );

    /**
     * Count of layer available for the FBO.
     * @return Layer count available.
     */
    uint getLayerCount() const;

    /**
     * Setting to draw the color texture or the depth texture of the FBO.
     * @param drawDepth If true the FBO will render the depth texture,
     *                  otherwise it will be the color texture.
     */
    void setDrawDepth( bool drawDepth );

    /**
     * Setting to change rendering colors to see duplication and overlaping.
     * @param debugColor True apply debug color, otherwise not apply the debug color.
     */
    void setDebugColor( bool debugColor );

    /**
     * Determine if the openGL functions are initialized.
     * @return True if the openGL functions are initialized, false otherwise.
     */
    bool isOpenGLInitialized() const;
    
    /**
     * To begin the automatic camera.
     */
    void playButtonEvent();

    /**
     * To pause the automatic camera.
     */
    void pauseButtonEvent();

    /**
     * To stop the automatic camera.
     */
    void stopButtonEvent();
    
    /**
     * Toggle display mode.
     */
    void toggleDisplayMode();

    /**
     * Change film size.
     */
    void setFilmType( const Film::Frame type );

    /**
     * Get film size.
     */
    Film::Frame getFilmType() const;

protected:

    /**
     * Initialize the openGL context.
     */
    void initializeGL() override;

    /**
     * Render a frame.
     */
    void paintGL() override;

    /**
     * Resize the viewport.
     * @param w New width.
     * @param h New height.
     */
    void resizeGL( int w, int h ) override;
    
    /**
     * Override of QWidget event.
     * Allow to start moving the camera.
     * 
     * @param event Data sent from Qt.
     */
    void mousePressEvent( QMouseEvent* event ) override;
    /**
     * Override of QWidget event.
     * Allow to move the camera.
     * 
     * @param event Data sent from Qt.
     */
    void mouseMoveEvent( QMouseEvent* event ) override;
    /**
     * Override of QWidget event.
     * Stop moving the camera.
     * 
     * @param event Data sent from Qt.
     */
    void mouseReleaseEvent( QMouseEvent* event ) override;
    
    /**
     * Override of QWidget event.
     * Start moving the camera.
     * 
     * @param event Data sent from Qt.
     */
    void keyPressEvent( QKeyEvent* event ) override;
    
    /**
     * Override of QWidget event.
     * Stop moving the camera.
     * 
     * @param event Data sent from Qt.
     */
    void keyReleaseEvent( QKeyEvent* event ) override;

protected slots:
    /** 
     * Update camera movement each frame. 
     * This update is called only if user is pressing a key.
     */
    void event_keyUpdate();

    /**
     * Move the camera.
     * Update the camera.
     */
    void cameraMove();

public:
    /** Optics settings (near, far, film, focus distance, ...). */
    OpticsSettings settings; // NOTE: Rename to comply with coding standards (please).

    //static constexpr uint POISSON_DISK_SAMPLES = 16;
    static constexpr uint POISSON_DISK_SAMPLES = 32;

private:
    /** Function to call after the initialization of OpenGL. */
    std::function<void()> _uiOnInitializeOpenGLCallback;

    /** Determine if the OpenGL functions are initialized. */
    bool _isOpenGLInitialized;

    /** View camera. */
    CameraFPS _cam;

    /** Poisson disk generator. Used to get evenly sparse lens samples. */
    PoissonDisk2D _poissonDisk;

    /** Screen. */
    ScreenMesh* _screen;

    /** If true the camera can move. */
    bool _mouseDown;
    
    /** Help to process the camera rotation. */
    QPointF _lastMousePosition;
    
    /** Memorize camera moving direction. */
    char _camDirection = 0;

    /** Timer to move in camera direction each frame. */
    QTimer* _tickKeyboard;
    
    /** The scene, containing every models and lights. */
    Scene _scene;

    /** Determine if the rendering will be with face filled or wireframed. */
    bool _drawFill;

    /** Modify rendering colors to see duplications and overlaping. */
    bool _fboDebugColor;

    /** Frame Buffer Object for layered rendering. */
    FrameBufferObject* _fbo;

    /** ID of the FBO layer to render. */
    uint _layerToRender;

    /** Determine if it will be the depth texture or the color texture to be rendered. */
    bool _drawDepth;

    /** The number of all the points in the routine. */
    GLint i;

    /** All the points of the routine. */
    QVector<glm::vec3> positions;

    /** All the rotations of the routine. */
    QVector<glm::vec3> angles;

    /** The new timer for the automatic camera. */
    QTimer* timerPlay;

    /** Determine the display mode: layered or composited. */
    bool _oneLayerMode;

    /** Decomposer class. Group any function related with the layered rendering. */
    Decomposition* _decomposition;

    /** Duplication functionalities (between decomposer and recomposer). */
    Duplication* _duplication;

    /** Recomposer class. Any function related to rendering multiview-synthetized image. */
    Recomposer* _recomposer;
};


#endif

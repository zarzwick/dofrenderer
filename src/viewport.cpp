#include "viewport.hpp"
#include "frameBufferObject/frameBufferObject.hpp"
#include "decomposition/decomposition.hpp"
#include "decomposition/duplication.hpp"

#include <vector>
#include <utility>
#include <iostream>
#include <limits>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <QMouseEvent>
#include <QDebug>
#include <QTimer>

using namespace std;


GLViewport::GLViewport ( QWidget* parent )
    : QOpenGLWidget { parent }
    , _isOpenGLInitialized { false }
    , _mouseDown { false }
    , _drawFill { true }
    , _fbo { nullptr }
    , _oneLayerMode{ false }
    , _decomposition { nullptr }
{
    setMouseTracking(true);

    setFocusPolicy(Qt::StrongFocus);

    _tickKeyboard = new QTimer{ this };
    if( _tickKeyboard )
    {
        _tickKeyboard->setInterval( 16 );
        connect(_tickKeyboard, SIGNAL(timeout()), this, SLOT(event_keyUpdate()));
    }

    _camDirection = 0;

    timerPlay = new QTimer();
    if(timerPlay)
    {
       timerPlay->setInterval(16);
       connect(timerPlay, SIGNAL(timeout()), this, SLOT(cameraMove()));
    }

    i = 0;

}

GLViewport::~GLViewport()
{
    if( _tickKeyboard != nullptr )
        delete _tickKeyboard;

    if( _fbo != nullptr )
        delete _fbo;

    if( _decomposition != nullptr )
        delete _decomposition;
}


void GLViewport::initializeGL()
{
    makeCurrent();

    QOpenGLWidget::initializeGL();

    // Resolve functions address to access OpenGL API
    initializeOpenGLFunctions();

    // Initialize decomposition functionnalities. It has its own shader system.
    _decomposition = new Decomposition;
    if( _decomposition == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create Decomposition.\n";

    // Same goes for recomposer.
    _recomposer = new Recomposer;
    if( _recomposer == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create Recomposer.\n";

    // Create the layered FBO.
    _layerToRender = 0;
    _drawDepth = false;
    constexpr size_t countFBOLayers = 10;
    _fbo = new FrameBufferObject( width(), height(), countFBOLayers );
    if( _fbo == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create layered FBO.\n";

    // Create duplication functionalities.
    _duplication = new Duplication( width(), height(), countFBOLayers );
    if( _duplication == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create duplicaiton functionalities.\n";

    // Create screen.
    _fbo->bindShader();
    _screen = new ScreenMesh;
    if( _screen == nullptr )
        std::cerr << "GLViewport::initializeGL : Failed to create screen mesh.\n";
    _screen->init();
    _fbo->unbindShader();

    // Create camera.
    _cam.SetPosition( glm::vec3{ 0.0f, 0.0f, -2.0f } );

    // And generate the lens' samples.
    //_poissonDisk.generate( GLViewport::POISSON_DISK_SAMPLES, 0.45, glm::vec2{0.f}, 1.f ); // for 16 samples.
    _poissonDisk.generate( GLViewport::POISSON_DISK_SAMPLES, 0.24, glm::vec2{0.f}, 1.f ); // for 32 samples.

    // Create light(s).
    _scene._lights.emplace_back( glm::vec3(0.f, 0.f, 30.f), glm::vec3(1.f, 1.f, 1.f), 16.f );
    _scene._lights.emplace_back( glm::vec3(50.f, 50.f, 0.f), glm::vec3(1.f, 1.f, 1.f), 16.f );
    _scene._lights.emplace_back( glm::vec3(10.f, -10.f, -22.f), glm::vec3(12.f, 12.f, 12.f), 16.f );

    // Prepare lights (e.g set uniform attributes).
    _decomposition->shader().bind();
    for ( unsigned int l = 0; l < _scene._lights.size(); ++ l )
    {
        _scene._lights[l].prepare( _decomposition->shader(), l );
    }
    _decomposition->shader().release();

    glEnable( GL_DEPTH_TEST );

    // Inform UI
    _uiOnInitializeOpenGLCallback();

    doneCurrent();

    _isOpenGLInitialized = true;
}


void GLViewport::paintGL()
{
    if( !_fbo )
    {
        std::cerr << "[paint] FBO is in an invalid state.\n";
        return;
    }

    if( !_decomposition )
    {
        std::cerr << "[paint] Decomposition functionnalities are in an invalid state.\n";
        return;
    }

    if( !_duplication )
    {
        std::cerr << "[paint] Duplication functionnalities are in an invalid state.\n";
        return;
    }

    if( !_recomposer )
    {
        std::cerr << "[paint] Recomposition functionnalities are in an invalid state.\n";
        return;
    }

    makeCurrent();

    // 1. Draw the scene off-screen, on FBO texture array.

    _fbo->bind();

    glClearColor( 0.3f, 0.3f, 0.3f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    if ( _drawFill )
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    else
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    const float&& near = settings.applyNearToWorld ? static_cast<float>( settings.getNearDistance(true) ) : 0.01f;
    const float&& far = settings.applyFarToWorld ? static_cast<float>( settings.getFarDistance(true) ) : 100.0f;

    const double&& layerCount = static_cast<double>(_fbo->getLayerCount());

    // Setup rendering matrices.
    const glm::mat4& view = _cam.getViewMatrix();
    const glm::mat4& projection = glm::perspective( glm::radians( _cam.getFOV() ), static_cast<GLfloat>( width() ) / static_cast<GLfloat>( height() ), near, far );

    _decomposition->bindShader(settings, _cam.getPosition(), layerCount);

    if ( !_decomposition->isShaderBound() )
        std::cerr << "[paint] GL shader unbound" << std::endl;

    // Send rendering matrices to the shader ( multiplications are faster on GPU than on CPU ).
    glUniformMatrix4fv( _decomposition->getShaderSettingPosition(Decomposition::View), 1, GL_FALSE, glm::value_ptr( view ) );
    glUniformMatrix4fv( _decomposition->getShaderSettingPosition(Decomposition::Projection), 1, GL_FALSE, glm::value_ptr( projection ) );
    glUniform1i( _decomposition->getShaderSettingPosition(Decomposition::LayerCount), _fbo->getLayerCount() );
    glUniform3fv( _decomposition->getShaderSettingPosition(Decomposition::CameraPosition), 1, glm::value_ptr( _cam.getPosition() ) );

    // Render scene (onto the fbo).
    for ( auto& m : _scene._models )
    {
        // Transformations matrices for model and normals
        glUniformMatrix4fv( _decomposition->getShaderSettingPosition(Decomposition::Model), 1, GL_FALSE, glm::value_ptr(m._transform) );
        glUniformMatrix4fv( _decomposition->getShaderSettingPosition (Decomposition::Normal), 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(m._transform))) );

        m.draw( &_decomposition->GetShaderProgram(), _scene._textures );
    }

    _decomposition->unbindShader();
    _fbo->unbind();


    _duplication->duplicate( settings, _decomposition, _fbo, _screen, _fboDebugColor );

    _fbo = _duplication->tradeFBO( _fbo );

    // 2. Draw on-screen, either in one-layer mode, or in recomposition mode.

    glClearColor( 0.f, 0.f, 0.f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

    if ( _oneLayerMode )
    {
        _fbo->bindShader( _layerToRender, _drawDepth );
        _screen->draw();
        _fbo->unbindShader();
    }
    else
    {
        _recomposer->bindShader( settings, _poissonDisk.data(), *_fbo, *_decomposition );
        _screen->draw();
        _recomposer->unbindShader();
    }

    doneCurrent();
}


void GLViewport::resizeGL( int w, int h )
{
    QOpenGLWidget::resizeGL( w, h );

    if( _fbo )
        _fbo->resize( w, h );
}


void GLViewport::setUIOnInitializeOpenGLCallback( const std::function<void()>& _callback )
{
    _uiOnInitializeOpenGLCallback = _callback;
}


uint GLViewport::addModel( const GLchar* file )
{
    makeCurrent();
    _scene.load( file );
    doneCurrent();
    update();
    _scene.bbox();
    return _scene._models.size() - 1;
}


void GLViewport::removeModel(uint model)
{
    makeCurrent();
    _scene._models.erase( _scene._models.begin() + model );
    doneCurrent();
    update();
    _scene.bbox();
}


void GLViewport::removeAll()
{
    makeCurrent();
    _scene._models.clear();
    doneCurrent();
    update();
}


void GLViewport::transformModel(uint modelID, glm::vec3 loc, glm::vec3 rot, glm::vec3 scal)
{
    _scene._models[modelID].transform( loc, rot, scal );
    update();
}


std::list<std::string> GLViewport::getModelsNames() const
{
    std::list<std::string> modelsNames;

    for ( Model const& model: _scene._models )
        modelsNames.emplace_back( model._name );
    
    return std::move( modelsNames );
}


void GLViewport::toggleWireframe(bool toggle)
{
    _drawFill = !toggle;
    update();
}


uint GLViewport::getMaxLayerCount() const
{
    if( _fbo )
        return _fbo->MAX_LAYER_COUNT;

    std::cerr << "GLViewport::getMaxLayerCount() : FBO Not initialied. \n";
    return 1;
}


uint GLViewport::setLayerCount( uint layerCount )
{
    if( _fbo )
    {
        makeCurrent();
        const uint clampedLayerCount = _fbo->setLayerCount( layerCount );
        doneCurrent();

        // Repaint OpenGL.
        update();

        return clampedLayerCount;
    }

    std::cerr << "GLViewport::getMaxLayerCount() : FBO Not initialied. \n";
    return 1;
}


uint GLViewport::getLayerCount() const
{
    if( _fbo )
        return _fbo->getLayerCount( );

    std::cerr << "GLViewport::getMaxLayerCount() : FBO Not initialied. \n";
    return 1;
}


uint GLViewport::setLayerToRender( uint layerToRender )
{
    if( !_fbo )
    {
        std::cerr << "GLViewport::getMaxLayerCount() : FBO Not initialied. \n";
        return 1;
    }

    if( layerToRender >= _fbo->MAX_LAYER_COUNT )
        layerToRender = _fbo->MAX_LAYER_COUNT - 1;


    _layerToRender = layerToRender;

    // Repaint OpenGL.
    update();

    return _layerToRender;
}


void GLViewport::setDrawDepth( bool drawDepth )
{
    _drawDepth = drawDepth;

    // Repain OpenGL.
    update();
}

void GLViewport::setDebugColor(bool debugColor)
{
    _fboDebugColor = debugColor;

    update();
}


void GLViewport::toggleDisplayMode()
{
    _oneLayerMode = ! _oneLayerMode;

    makeCurrent();
    update();
    doneCurrent();
}


void GLViewport::setFilmType( const Film::Frame type )
{
    settings.setFilmType( type );
}


Film::Frame GLViewport::getFilmType() const
{
    return settings.getFilmType();
}


void GLViewport::mousePressEvent( QMouseEvent* event )
{
    _mouseDown = true;
    _lastMousePosition = event->localPos();
}


void GLViewport::mouseMoveEvent( QMouseEvent* event )
{
    if( _mouseDown && ( event->buttons() & Qt::LeftButton ) )
    {
        const QPointF& newPosition = event->localPos();

        const QPointF offset{ _lastMousePosition - newPosition };

        _cam.processMouseMove( glm::vec2{ offset.x(), offset.y() }, true );

        // qDebug() << "Turn " << (_lastMousePosition - newPosition) << "\n";

        _lastMousePosition = newPosition;

        // Repaint opengl.
        update();
    }
}


bool GLViewport::isOpenGLInitialized() const
{
    return _isOpenGLInitialized;
}

void GLViewport::mouseReleaseEvent( QMouseEvent* )
{
    _mouseDown = false;
}


void GLViewport::keyPressEvent ( QKeyEvent* event )
{
    if( event->key() == Qt::Key_Z || event->key() == Qt::Key_W )
        _camDirection = _camDirection | CameraFPS::Forward;
    if( event->key() == Qt::Key_S )
        _camDirection = _camDirection | CameraFPS::Backward;
    if( event->key() == Qt::Key_D )
        _camDirection = _camDirection | CameraFPS::CameraFPS::Right;
    if( event->key() == Qt::Key_Q || event->key() == Qt::Key_A )
        _camDirection = _camDirection | CameraFPS::Left;



    if( _tickKeyboard )
        _tickKeyboard->start();
}


void GLViewport::event_keyUpdate()
{
    if( _camDirection != 0 )
        _cam.processKeyboard( static_cast<CameraFPS::Direction>( _camDirection ), 0.016f );

    // Repaint opengl.
    update();
}


void GLViewport::keyReleaseEvent( QKeyEvent* event )
{
    if( _tickKeyboard )
        _tickKeyboard->stop();

    if( event->key() == Qt::Key_Z  || event->key() == Qt::Key_W )
    {
        const char mask = ~(CameraFPS::Forward);
        _camDirection = _camDirection & mask;
    }
    if( event->key() == Qt::Key_S )
    {
        const char mask = ~(CameraFPS::Backward);
        _camDirection = _camDirection & mask;
    }
    if( event->key() == Qt::Key_D )
    {
        const char mask = ~(CameraFPS::Right);
        _camDirection = _camDirection & mask;
    }
    if( event->key() == Qt::Key_Q || event->key() == Qt::Key_A )
    {
        const char mask = ~(CameraFPS::Left);
        _camDirection = _camDirection & mask;
    }
}

void GLViewport::cameraMove()
{
    if (i == 0)
    {
        _cam.SetPosition(positions[i]);
        i = i+1;
    }
    else if (i < 1002)
    {
        _cam.SetPosition(positions[i]);
        _cam.rotate(angles[i],true);
        i = i+1;
    }
    else
    {
        i = 0;
        _cam.SetPosition(positions[0]);
        i = i+1;
    }

    update();
}

void GLViewport::playButtonEvent()
{
    timerPlay->start();

    const glm::vec3 p1{ 0.0f, 0.0f, -3.0f };
    const glm::vec3 p2{ 1.5f, -1.5f, -3.0f };
    const glm::vec3 p3{ 3.0f, 0.0f, -3.0f };
    const glm::vec3 p4{ 3.0f, 1.5f, -1.5f };
    const glm::vec3 p5{ 3.0f, 0.0f, 0.0f };
    const glm::vec3 p6{ 3.0f, -1.5f, 1.5f };
    const glm::vec3 p7{ 3.0f, 0.0f, 3.0f};
    const glm::vec3 p8{ 1.5f, 1.5f, 3.0f };
    const glm::vec3 p9{ 0.0f, 0.0f, 3.0f };
    const glm::vec3 p10{ -1.5f, -1.5f, 3.0f };
    const glm::vec3 p11{ -3.0f, 0.0f, 3.0f };
    const glm::vec3 p12{ -3.0f, 1.5f, 1.5f };
    const glm::vec3 p13{ -3.0f, 0.0f, 0.0f };
    const glm::vec3 p14{ -3.0f, -1.5f, -1.5f };
    const glm::vec3 p15{ -3.0f, 0.0f, -3.0f };
    const glm::vec3 p16{ -1.5f, 1.5f, -3.0f };

    for (double t = 0.0; t <= 1.0; t += 0.001)
    {
        double a1 = pow((1 - t), 16);
        double a2 = pow((1 - t), 15) * t * 16;
        double a3 = pow((1 - t), 14) * pow(t , 2) * 120;
        double a4 = pow((1 - t), 13) * pow(t , 3) * 560;
        double a5 = pow((1 - t), 12) * pow(t , 4) * 1820;
        double a6 = pow((1 - t), 11) * pow(t , 5) * 4368;
        double a7 = pow((1 - t), 10) * pow(t , 6) * 8008;
        double a8 = pow((1 - t), 9) * pow(t , 7) * 11440;
        double a9 = pow((1 - t), 8) * pow(t , 8) * 11870;
        double a10 = pow((1 - t), 7) * pow(t , 9) * 11440;
        double a11 = pow((1 - t), 6) * pow(t , 10) * 8008;
        double a12 = pow((1 - t), 5) * pow(t , 11) * 4368;
        double a13 = pow((1 - t), 4) * pow(t , 12) * 1820;
        double a14 = pow((1 - t), 3) * pow(t , 13) * 560;
        double a15 = pow((1 - t), 2) * pow(t , 14) * 120;
        double a16 = pow((1 - t), 1) * pow(t , 15) * 16;
        double a17 = pow(t , 16) ;

        glm::vec3 position;
        position.x = a1*p1.x + a2*p2.x + a3*p3.x + a4*p4.x + a5*p5.x + a6*p6.x + a7*p7.x + a8*p8.x + a9*p9.x + a10*p10.x + a11*p11.x + a12*p12.x + a13*p13.x + a14*p14.x + a15*p15.x + a16*p16.x + a17*p1.x;
        position.y = a1*p1.y + a2*p2.y + a3*p3.y + a4*p4.y + a5*p5.y + a6*p6.y + a7*p7.y + a8*p8.y + a9*p9.y + a10*p10.y + a11*p11.y + a12*p12.y + a13*p13.y + a14*p14.y + a15*p15.y + a16*p16.y + a17*p1.y;
        position.z = a1*p1.z + a2*p2.z + a3*p3.z + a4*p4.z + a5*p5.z + a6*p6.z + a7*p7.z + a8*p8.z + a9*p9.z + a10*p10.z + a11*p11.z + a12*p12.z + a13*p13.z + a14*p14.z + a15*p15.z + a16*p16.z + a17*p1.z;
        positions.push_back(position);

        glm::vec3 angle;
        angle.x = 0.0f;
        angle.y = 360.0f/1000;
        angle.z = 0.0f;
        angles.push_back(angle);
    }

    /*const glm::vec3 p1{ 0.0f, 0.0f, -2.0f };
    const glm::vec3 p2{ 1.0f, -1.0f, -2.0f };
    const glm::vec3 p3{ 2.0f, 0.0f, -2.0f };
    const glm::vec3 p4{ 2.0f, 1.0f, -1.0f };
    const glm::vec3 p5{ 2.0f, 0.0f, 0.0f };
    const glm::vec3 p6{ 2.0f, -1.0f, 1.0f };
    const glm::vec3 p7{ 2.0f, 0.0f, 2.0f};
    const glm::vec3 p8{ 1.0f, 1.0f, 2.0f };
    const glm::vec3 p9{ 0.0f, 0.0f, 2.0f };
    const glm::vec3 p10{ -1.0f, -1.0f, 2.0f };
    const glm::vec3 p11{ -2.0f, 0.0f, 2.0f };
    const glm::vec3 p12{ -2.0f, 1.0f, 1.0f };
    const glm::vec3 p13{ -2.0f, 0.0f, 0.0f };
    const glm::vec3 p14{ -2.0f, -1.0f, -1.0f };
    const glm::vec3 p15{ -2.0f, 0.0f, -2.0f };
    const glm::vec3 p16{ -1.0f, 1.0f, -2.0f };

    glm::vec3 position;
    glm::vec3 angle;
    double t;

    for ( t = 0.0; t < 0.25; t += 0.001)
    {
        double a1 = pow((1 - t), 3) ;
        double a2 = pow((1 - t), 2) * pow(t , 1) * 4;
        double a3 = pow((1 - t), 1) * pow(t , 2) * 6;
        double a4 = pow((1 - t), 1) * pow(t , 3) * 4;
        double a5 = pow(t , 4) ;

        position.x = a1*p1.x + a2*p2.x + a3*p3.x + a4*p4.x + a5*p5.x;
        position.y = a1*p1.y + a2*p2.y + a3*p3.y + a4*p4.y + a5*p5.y;
        position.z = a1*p1.z + a2*p2.z + a3*p3.z + a4*p4.z + a5*p5.z;
        positions.push_back(position);

        angle.x = -position.y;
        angle.y = 90.f - t*360.0f/1000;
        angle.z = 0.0f;
        angles.push_back(angle);
    }
    for ( t = 0.25; t < 0.5; t += 0.001)
    {
        double a1 = pow((1 - t), 3) ;
        double a2 = pow((1 - t), 2) * pow(t , 1) * 4;
        double a3 = pow((1 - t), 1) * pow(t , 2) * 6;
        double a4 = pow((1 - t), 1) * pow(t , 3) * 4;
        double a5 = pow(t , 4) ;

        position.x = a1*p5.x + a2*p6.x + a3*p7.x + a4*p8.x + a5*p9.x;
        position.y = a1*p5.y + a2*p6.y + a3*p7.y + a4*p8.y + a5*p9.y;
        position.z = a1*p5.z + a2*p6.z + a3*p7.z + a4*p8.z + a5*p9.z;
        positions.push_back(position);

        angle.x = -position.y;
        angle.y = 90.f - t*360.0f/1000;
        angle.z = 0.0f;
        angles.push_back(angle);
    }
    for ( t = 0.5; t < 0.75; t += 0.001)
    {
        double a1 = pow((1 - t), 3) ;
        double a2 = pow((1 - t), 2) * pow(t , 1) * 4;
        double a3 = pow((1 - t), 1) * pow(t , 2) * 6;
        double a4 = pow((1 - t), 1) * pow(t , 3) * 4;
        double a5 = pow(t , 4) ;

        position.x = a1*p9.x + a2*p10.x + a3*p11.x + a4*p12.x + a5*p13.x;
        position.y = a1*p9.y + a2*p10.y + a3*p11.y + a4*p12.y + a5*p13.y;
        position.z = a1*p9.z + a2*p10.z + a3*p11.z + a4*p12.z + a5*p13.z;
        positions.push_back(position);

        angle.x = -position.y;
        angle.y = 90.f - t*360.0f/1000;
        angle.z = 0.0f;
        angles.push_back(angle);
    }
    for ( t = 0.75; t < 1; t += 0.001)
    {
        double a1 = pow((1 - t), 3) ;
        double a2 = pow((1 - t), 2) * pow(t , 1) * 4;
        double a3 = pow((1 - t), 1) * pow(t , 2) * 6;
        double a4 = pow((1 - t), 1) * pow(t , 3) * 4;
        double a5 = pow(t , 4) ;

        position.x = a1*p13.x + a2*p14.x + a3*p15.x + a4*p16.x + a5*p1.x;
        position.y = a1*p13.y + a2*p14.y + a3*p15.y + a4*p16.y + a5*p1.y;
        position.z = a1*p13.z + a2*p14.z + a3*p15.z + a4*p16.z + a5*p1.z;
        positions.push_back(position);

        angle.x = -position.y;
        angle.y = 90.f - t*360.0f/1000;
        angle.z = 0.0f;
        angles.push_back(angle);
    }*/
}

void GLViewport::pauseButtonEvent()
{
    timerPlay->stop();

    _cam.getPosition();

    update();
}

void GLViewport::stopButtonEvent()
{
    timerPlay->stop();

    _cam.SetPosition(glm::vec3{0.0f, 0.0f, -2.0f});
    _cam.setRotation(glm::vec3{0.0f, 90.0f, 0.0f}, true);


    i = 0;

    update();
}


/* Useful links:
 * [1] http://doc.qt.io/qt-5/qopenglwidget.html#details
 * [2] http://doc.qt.io/qt-5/qsurfaceformat.html#FormatOption-enum
 * [3] http://doc.qt.io/qt-5/qtgui-index.html
 * [4] http://doc.qt.io/qt-5/qabstractopenglfunctions.html#details
 * [5] https://learnopengl.com/
 */

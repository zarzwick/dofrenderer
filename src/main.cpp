#include "dofrenderer.hpp"

#include <QDebug>
#include <QStringList>

#include <QDir>
#include <QApplication>
#include <QCommandLineParser>

#include <QSurface>
#include <QSurfaceFormat>


int main(int argc, char *argv[])
{
    // openGL version (4.1), Core, etc...
    QSurfaceFormat surfaceFormat;
    surfaceFormat.setVersion( 4, 1 );
    surfaceFormat.setProfile( QSurfaceFormat::CoreProfile );

    // macOS and some systems need the default surface format to be set
    // See last paragraph of http://doc.qt.io/qt-5/qopenglwidget.html#details
    QSurfaceFormat::setDefaultFormat( surfaceFormat );


    // Declare Qt application
    QApplication app(argc, argv);

    QCoreApplication::setApplicationName( argv[0] );
    QCoreApplication::setApplicationVersion( "0.1" );

    
    // Deal with current directory
    QDir::setCurrent( QCoreApplication::applicationDirPath() );
    

    // Manage options
    QCommandLineOption sceneOption( QStringList() << "s" << "scene", "Scene file", "" );

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption( sceneOption );

    parser.process( app );


    // Setup and start application
    DOFRenderer w;
    w.show();

    return app.exec();
}


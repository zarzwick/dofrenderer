#ifndef M1_DOFRDR_H
#define M1_DOFRDR_H

#include "viewport.hpp"
#include "camera/paraphysique.hpp"

#include <QMainWindow>
#include <QOpenGLWidget>
#include <QListWidgetItem>


class PopUpAbout;
class PopUpHelp;

namespace Ui {
class DOFRenderer;
}


/**
 * Global window displaying viewport and widgets.
 */
class DOFRenderer : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * Default constructor.
     * @param parent Parent of the widget. Optional.
     */
    explicit DOFRenderer( QWidget *parent = 0 );
    
    /** Destructor. */
    ~DOFRenderer();

    /**
     * Callback called when viewport is initialized
     * and still own the OpenGL context.
     * @param ui To be filled with "this" class.
     */
    static void onInitializeOpenGL( DOFRenderer* ui );

protected:
    struct ModelItem : QListWidgetItem
    {
        
        // WARNING, currently a big big BIG confusion is done between a model and a scene in the listing system.
        
        ModelItem( const QString& label, uint id, QListWidget* parent = nullptr )
            : QListWidgetItem { label, parent }
            , _modelID { id }
        {}
        
        uint _modelID;
    };

private slots:
    
    // Looking around
    
    void on_playButton_clicked();

    void on_pauseButton_clicked();
    
    void on_stopButton_clicked();

    // Object management

    void on_addModelButton_clicked();
    
    void on_removeModelButton_clicked();

    void on_reloadSceneButton_clicked();
    
    // Layered rendering settings
    
    void on_radioButtonOneLayer_toggled(bool checked);

    void on_radioButtonRecompose_toggled(bool checked);

    void on_toggleWireframe_toggled(bool toggle);

    void on_layerCountSlider_valueChanged(int value);

    void on_layerToRenderSlider_valueChanged(int value);

    void on_drawDepthCheckbox_toggled(bool checked);

    void on_debugColorCheckbox_toggled(bool checked);

    // Optical settings

    void on_spinBoxFocusDistance_editingFinished();

    void on_sliderAperture_valueChanged(int value);

    void on_spinBoxFocalLength_editingFinished();

    void on_spinBoxFar_editingFinished();

    void on_spinBoxNear_editingFinished();

    void on_checkBoxFar_toggled(bool checked);

    void on_checkBoxNear_toggled(bool checked);

    void on_framePresetComboBox_currentIndexChanged(int index);


    // Object properties
    
    void on_objPropPosX_valueChanged(double x);
    
    void on_objPropPosY_valueChanged(double y);
    
    void on_objPropPosZ_valueChanged(double z);
    
    void on_objPropRotX_valueChanged(double x);
    
    void on_objPropRotY_valueChanged(double y);
    
    void on_objPropRotZ_valueChanged(double z);
    
    void on_objPropScalX_valueChanged(double x);
    
    void on_objPropScalY_valueChanged(double y);
    
    void on_objPropScalZ_valueChanged(double z);

    // '?' Menu

    void on_actionAbout_triggered();

    void on_actionHelp_triggered();

private:
    void transformMatrixChanged();

    void rebuildModelList();

    // Optics settings UI updates.
    void updateFocusDistanceUI();
    void updateFocalLengthUI();
    void updateApertureUI();
    void updateFarUI();
    void updateNearUI();
    
private:
    Ui::DOFRenderer *ui;
    //OpticsSettings setting;

    PopUpAbout* _popUpAbout;
    PopUpHelp* _popUpHelp;
};

#endif // M1DEMO_DOFRENDERER_H

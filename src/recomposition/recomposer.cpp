#include "recomposer.hpp"

#include <iostream>


Recomposer::Recomposer()
{
    // Fetch OpenGL function set.
    QAbstractOpenGLFunctions* af = QOpenGLContext::currentContext()->versionFunctions();
    _gl = static_cast<QOpenGLFunctions_4_1_Core*>( af );

    // Create shader program.
    initShader();
}


void Recomposer::bindShader( OpticsSettings& lens, std::vector<glm::vec2> lensSamples,
                             FrameBufferObject& fbo, Decomposition& dec )
{
    _shader.bind();

    const glm::vec2 pixelDefinition = glm::vec2(fbo.getWidth(), fbo.getHeight()) / lens.getFilmSize();

    // Prepare all the uniforms needed (quite a lot)
    glAssert( _gl->glUniform1ui(_location[W], fbo.getWidth()) );
    glAssert( _gl->glUniform1ui(_location[H], fbo.getHeight()) );
    glAssert( _gl->glUniform2f(_location[PIXMM], pixelDefinition.x, pixelDefinition.y) );
    glAssert( _gl->glUniform1f(_location[DF], lens.getFocusDistance()) );
    glAssert( _gl->glUniform1f(_location[FOCAL], lens.getFocalLength()) );
    glAssert( _gl->glUniform1f(_location[DNEAR], lens.getNearDistance()) );
    glAssert( _gl->glUniform1f(_location[DFAR], lens.getFarDistance()) );
    glAssert( _gl->glUniform1f(_location[LENS_RADIUS], lens.getLensRadius()) );
    glAssert( _gl->glUniform1i(_location[NB_LAYERS], fbo.getLayerCount()) );
    glAssert( _gl->glUniform1i(_location[COLOR_LAYERS], TEXTURE_COLOR) );
    glAssert( _gl->glUniform1i(_location[DEPTH_LAYERS], TEXTURE_DEPTH) );
    glAssert( _gl->glUniform1i(_location[NB_SAMPLES], lensSamples.size()) );
    glAssert( _gl->glUniform2fv(_location[LENS_SAMPLES], lensSamples.size(), (const GLfloat*) lensSamples.data()) );

    // Precompute the layers distances
    float layerDepths[FrameBufferObject::MAX_LAYER_COUNT];

    for ( uint l = 0; l < fbo.getLayerCount(); ++ l )
        layerDepths[l] = layerDepth( l, lens, fbo, dec );

    glAssert( _gl->glUniform1fv(_location[LAYERS_DISTANCES], fbo.getLayerCount(), &layerDepths[0]) );

    // And textures, too
    glAssert( _gl->glActiveTexture(GL_TEXTURE0+TEXTURE_COLOR) );
    glAssert( _gl->glBindTexture(GL_TEXTURE_2D_ARRAY, fbo.getColorTexture()) );
    glAssert( _gl->glActiveTexture(GL_TEXTURE0+TEXTURE_DEPTH) );
    glAssert( _gl->glBindTexture(GL_TEXTURE_2D_ARRAY, fbo.getDepthTexture()) );
}


void Recomposer::unbindShader()
{
    glAssert( _gl->glActiveTexture( GL_TEXTURE0+TEXTURE_COLOR ) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, 0 ) );
    glAssert( _gl->glActiveTexture( GL_TEXTURE0+TEXTURE_DEPTH ) );
    glAssert( _gl->glBindTexture( GL_TEXTURE_2D_ARRAY, 0 ) );

    glAssert( _gl->glActiveTexture(GL_TEXTURE0) );

    _shader.release();
}


void Recomposer::initShader()
{
    // Create the shader program

    if ( ! _shader.addShaderFromSourceFile(QOpenGLShader::Vertex, "../shaders/recompose.glsl.vert") )
        std::cerr << "[recomposer] Failed to find vertex shader." << std::endl;

    if ( ! _shader.addShaderFromSourceFile(QOpenGLShader::Fragment, "../shaders/recompose.glsl.frag") )
        std::cerr << "[recomposer] Failed to find fragment shader." << std::endl;

    if ( ! _shader.link() )
        std::cerr << "[recomposer] Failed to link recomposer shader.";

    // Precompute every uniform locations

    GLuint shader = _shader.programId();

    _location[W] =              _gl->glGetUniformLocation( shader, "w" );
    _location[H] =              _gl->glGetUniformLocation( shader, "h" );
    _location[PIXMM] =          _gl->glGetUniformLocation( shader, "pixmm" );
    _location[DF] =             _gl->glGetUniformLocation( shader, "df" );
    _location[FOCAL] =          _gl->glGetUniformLocation( shader, "focal" );
    _location[DNEAR] =          _gl->glGetUniformLocation( shader, "dNear" );
    _location[DFAR] =           _gl->glGetUniformLocation( shader, "dFar" );
    _location[LENS_RADIUS] =    _gl->glGetUniformLocation( shader, "lensRadius" );
    _location[LENS_SAMPLES] =   _gl->glGetUniformLocation( shader, "lensSamples" );
    _location[NB_SAMPLES] =     _gl->glGetUniformLocation( shader, "NB_SAMPLES" );
    _location[NB_LAYERS] =      _gl->glGetUniformLocation( shader, "NB_LAYERS" );
    _location[LAYERS_DISTANCES]=_gl->glGetUniformLocation( shader, "layerDistance" );
    _location[COLOR_LAYERS] =   _gl->glGetUniformLocation( shader, "layersColor" );
    _location[DEPTH_LAYERS] =   _gl->glGetUniformLocation( shader, "layersDepth" );
}

float Recomposer::layerDepth( uint l, OpticsSettings& lens, FrameBufferObject& fbo, Decomposition& dec )
{
    // Formula derived from (4) in the paper.
    return 1/((1/lens.getNearDistance()) - ((l*dec.getDelta())/(lens.getFocusDistance()*dec.getK())));
}


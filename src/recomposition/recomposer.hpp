#ifndef M1_DOFRDR_RECOMPOSER_HPP
#define M1_DOFRDR_RECOMPOSER_HPP

#include "../opengl.hpp"
#include "../camera/paraphysique.hpp"
#include "../frameBufferObject/frameBufferObject.hpp"
#include "../decomposition/decomposition.hpp"

#include <glm/glm.hpp>

#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_4_1_Core>


/**
 * Recompose the scene from a framebuffer. This class has the sole purpose of containing the
 * associated shader program, and providing a bindShader() method. The effective gl call will
 * be realized by the viewport.
 * This choice of implementation is mostly to comply with decomposition's (plus it does indeed
 * work as any other solution.)
 */
class Recomposer {

public:
    /** Constructor. Load the shader program and link it. */
    Recomposer();

    /**
     * Bind the shader. Also, prepare all the uniform informations.
     * @param lens The lens' physical properties.
     * @param lensSamples A set of 2D points telling where to sample the lens.
     * @param fbo The frameBuffer Object, for its bound textures and some properties.
     * @param dec Decomposer that rules the layers' depths.
     */
    void bindShader( OpticsSettings& lens, std::vector<glm::vec2> lensSamples,
                     FrameBufferObject& fbo, Decomposition& dec );

    /** Unbind the shader. */
    void unbindShader();

private:
    /** Create the shader program and link it. */
    void initShader();

    /**
     * Compute the depth of a layer's lower bound.
     * NOTE This could benefit from being relocated elsewhere.
     * @param l The layer you are looking for.
     * @param lens The lens' physical properties.
     * @param fbo The frameBuffer Object, for its bound textures and some properties.
     * @param dec Decomposer that rules the layers' depths.
     * @return The lower bound of l.
     */
    float layerDepth( uint l, OpticsSettings& lens, FrameBufferObject& fbo, Decomposition& dec );

private:
    enum UniformLocation {
        W = 0,
        H,
        PIXMM,
        DF,
        FOCAL,
        DNEAR,
        DFAR,
        LENS_RADIUS,
        LENS_SAMPLES,
        NB_SAMPLES,
        NB_LAYERS,
        LAYERS_DISTANCES,
        COLOR_LAYERS,
        DEPTH_LAYERS,
        UNIFORM_COUNT
    };

    enum TextureUnit {
        TEXTURE_COLOR = 0,
        TEXTURE_DEPTH,
        TEXTURE_COUNT
    };

    /** Different uniform locations. */
    GLuint _location[UNIFORM_COUNT];

    /** The aforementionned shader program. */
    QOpenGLShaderProgram _shader;

    /** OpenGL functions access. */
    QOpenGLFunctions_4_1_Core* _gl;

}; // end Recomposer


#endif

